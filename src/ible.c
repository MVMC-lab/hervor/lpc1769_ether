/**
 * @file ible.c
 * @author LiYu87@mvmc-lab
 * @brief Interface between LWIP and Ethernet MAC
 * @date 2020.04.08
 *
 * TODO: zero copy mode?
 */

#include "ible.h"

#include <lwip/etharp.h>
#include <lwip/netif.h>
#include <lwip/opt.h>
#include <lwip/pbuf.h>
#include <string.h>

#include "enet.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/
typedef struct __ible_enet_handler {
    EnetRxStat_t *rxs_p;   /**< Pointer to RX status list */
    EnetRxDesc_t *rxd_p;   /**< Pointer to RX descriptor list */
    EnetTxStat_t *txs_p;   /**< Pointer to TX status list */
    EnetTxDesc_t *txd_p;   /**< Pointer to TX descriptor list */
    struct netif *netif_p; /**< Reference back to LWIP parent netif */
    uint8_t mii_phy_id;    /**< Ethernet MII Physical ID */
} IbleEnetHandler_t;

IbleEnetHandler_t IbleEnetHandlerStr;

/*****************************************************************************
 * Private functions
 ****************************************************************************/
static err_t ible_etharp_output(struct netif *netif, struct pbuf *q,
                                const ip_addr_t *ipaddr) {
    /* Only send packet is link is up */
    if (netif->flags & NETIF_FLAG_LINK_UP) {
        return etharp_output(netif, q, ipaddr);
    }

    return ERR_CONN;
}

err_t ible_low_level_output(struct netif *netif_p, struct pbuf *p) {
    IbleEnetHandler_t *eth_p = netif_p->state;
    uint32_t tx_idx = TxProduceIndex;

    // TODO: handle ERROR here

    uint8_t *buffer = (uint8_t *)(eth_p->txd_p[tx_idx].Packet);

    /* copy frame from pbufs to driver buffers */
    if (p->len < ENET_ETH_MAX_FLEN) {
        memcpy(buffer, p->payload, p->len);

        eth_p->txd_p[tx_idx].Control =
            (p->len - 1) | ENET_TCTRL_LAST_MASK;
        TxProduceIndex = (tx_idx + 1) % ENET_NUM_TX_DESCS;
    }
    else {
        // TODO: handle packet > ENET_ETH_MAX_FLEN
        LWIP_DEBUGF(NETIF_DEBUG, ("low_level_output: err1\n"));
    }
    return ERR_OK;
}

static uint8_t is_rx_have_data() {
    return (RxProduceIndex != RxConsumeIndex) ? 1 : 0;
}

static struct pbuf *ible_low_level_intput(struct netif *netif_p) {
    struct pbuf *p = NULL;

    IbleEnetHandler_t *eth_p = netif_p->state;

    uint16_t len = 0;
    uint8_t *buffer;
    uint32_t payloadoffset = 0;
    uint32_t rx_idx = 0;
    uint8_t rxd_num = 0;
    uint32_t i = 0;
    uint32_t fragment_len = 0;

    if (!is_rx_have_data()) {
        return NULL;
    }

    // TODO: check else error type
    // e.g. RXOVERRUN
    rx_idx = RxConsumeIndex;
    /**
     * get length
     *  + 1 for "-1 encoded"
     */
    rxd_num = 1;
    for (i = 0; i < rxd_num; i++) {
        len += (eth_p->rxs_p[rx_idx + i].StatusInfo &
                ENET_RCTRL_SIZE_MASK) +
               1;
    }

    if (len > 0) {
        /* We allocate a pbuf chain of pbufs from the Lwip buffer pool
         */
        p = pbuf_alloc(PBUF_RAW, len, PBUF_POOL);
    }
    else {
        // TODO: ERROR here
        LWIP_DEBUGF(NETIF_DEBUG, ("rx len is 0\n"));
        RxConsumeIndex = (RxConsumeIndex + 1) % ENET_NUM_RX_DESCS;
        return NULL;
    }

    if (p == NULL) {
        // TODO: ERROR here
        LWIP_DEBUGF(NETIF_DEBUG,
                    ("can't alloc memory for rx pbuf\n"));
        RxConsumeIndex = (RxConsumeIndex + 1) % ENET_NUM_RX_DESCS;
        return NULL;
    }

    for (i = 0; i < rxd_num; i++) {
        buffer = (void*)eth_p->rxd_p[rx_idx + i].Packet;
        fragment_len = (eth_p->rxs_p[rx_idx + i].StatusInfo &
                        ENET_RCTRL_SIZE_MASK) +
                       1;

        memcpy((uint8_t *)((uint8_t *)p->payload + payloadoffset),
               (uint8_t *)((uint8_t *)buffer), fragment_len);
        payloadoffset += fragment_len;
    }

    RxConsumeIndex = (RxConsumeIndex + rxd_num) % ENET_NUM_RX_DESCS;

    return p;
}

/*****************************************************************************
 * Public functions
 ****************************************************************************/
void ible_input(struct netif *netif_p) {
    struct pbuf *p = NULL;
    err_t err;
    p = ible_low_level_intput(netif_p);

    if (p == NULL) {
        return;
    }
    err = netif_p->input(p, netif_p);

    if (err != ERR_OK) {
        LWIP_DEBUGF(NETIF_DEBUG,
                    ("ethernetif_input: IP input error\n"));
        pbuf_free(p);
        p = NULL;
    }
}

static uint8_t ible_low_level_init(struct netif *netif_p) {
    IbleEnetHandler_t *eth_p = netif_p->state;

    enet_init();
    enet_mii_init();
    enet_phy_reset(eth_p->mii_phy_id);

    enet_descriptors_init();

    enet_tx_enable();
    enet_rx_enable();

    enet_set_addr(netif_p->hwaddr);

    eth_p->rxd_p = RxDescs;
    eth_p->rxs_p = RxStats;
    eth_p->txd_p = TxDescs;
    eth_p->txs_p = TxStats;

    return 0;
}

err_t ible_netif_init(struct netif *netif_p) {
    uint8_t err = 0;

    LWIP_ASSERT("netif_p != NULL", (netif_p != NULL));

    IbleEnetHandlerStr.netif_p = netif_p;
    netif_p->state = (void *)&IbleEnetHandlerStr;

    /* set MAC hardware address */
    netif_p->hwaddr[0] = 0x00;
    netif_p->hwaddr[1] = 0x60;
    netif_p->hwaddr[2] = 0x37;
    netif_p->hwaddr[3] = 0x12;
    netif_p->hwaddr[4] = 0x34;
    netif_p->hwaddr[5] = 0x56;
    netif_p->hwaddr_len = 6;

    /* maximum transfer unit */
    netif_p->mtu = 1500;

    /* device capabilities */
    netif_p->flags = NETIF_FLAG_BROADCAST | NETIF_FLAG_ETHARP |
                     NETIF_FLAG_UP | NETIF_FLAG_ETHERNET;

    /* Initialize the hardware */
    err = ible_low_level_init(netif_p);
    if (err != 0) {
        return err;
    }

#if LWIP_NETIF_HOSTNAME
    /* Initialize interface hostname */
    netif_p->hostname = "asa_m3_v1";
#endif /* LWIP_NETIF_HOSTNAME */

    netif_p->name[0] = 'e';
    netif_p->name[1] = 'n';

    netif_p->output = ible_etharp_output;
    netif_p->linkoutput = ible_low_level_output;

    // TODO: OS thread initialize here
    return 0;
}
