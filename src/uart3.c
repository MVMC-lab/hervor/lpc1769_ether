/**
 * @file uart3.c
 * @author LiYu87@mvmc-lab
 * @brief uart3 driver for stdio
 * @date 2020.02.17
 */

#include "uart3.h"

#include "LPC1769.h"
#include "regdef.h"

void uart3_putc(uint8_t data) {
    while (!(U3LSR & USART_LSR_THRE)) {
        ;
    }
    U3THR = data;
    while (!(U3LSR & USART_LSR_TEMT)) {
        ;
    }
}

uint8_t uart3_getc(void) {
    while (!(U3LSR & USART_LSR_RDR)) {
        ;
    }
    return U3RBR;
}
