/**
 * @file syscall.c
 * @author LiYu87@mvmc-lab
 * @brief Provide _sys_ I/O functions
 * @date 2020.04.14
 *
 * 環境為 nosys ，不採用c standard lib部分函式，使用nano-lib。
 * 以下為 stdio 的 printf, scanf 所需要實現的函式。
 */

#include "uart3.h"

int _write (int fd __attribute__((unused)), char *ptr, int len) {
    int i;
    for(i = 0; i< len; i++) {
        uart3_putc(*ptr++);
    }
    return len;
}

int _read (int fd  __attribute__((unused)), char *ptr, int len) {
    int i;
    for(i = 0; i< len; i++) {
        *ptr++ = uart3_getc();
    }
    return len;
}

void _ttywrch(int ch) {
    uart3_putc(ch);
}
