/**
 * @file system.c
 * @author LiYu87@mvmc-lab
 * @date 2020.03.03
 * @brief system initailize and deinitailize function.
 */

#include "system.h"

#include <LPC1769.h>
#include <stdio.h>

#include "regdef.h"
#include "syst.h"

/**************************************************************
 * Macros                                                     *
 **************************************************************/
// system clock used macros
#define PLL0_M 20
#define PLL0_N 1
#define CPU_CLK_DEVIDER 4

// uart3 clock used macros
#define MUL_VAL 4
#define DIVADD_VAL 1

#define DLL_VAL 39
#define DLM_VAL 0

/**************************************************************
 * static functions declare                                   *
 **************************************************************/
/**
 * @brief 初始化 clock 硬體
 *
 * 1. 設定 clk_pll0 為 480 MHz
 * 2. 設定 clk_cpu 為 120 MHz
 */
static void system_clock_init(void);

/**
 * @brief 反初始化 clock 硬體
 */
static void system_clock_deinit(void);

/**
 * @brief 初始化 power 硬體
 *
 * 1. gpio
 * 2. uart3
 * 3. enet
 */
static void system_power_init(void);

/**
 * @brief 反初始化 power 硬體
 */
static void system_power_deinit(void);

/**
 * @brief 初始化 pinconnect 硬體
 *
 * 1. uart3
 * 2. enet
 */
static void system_pinconnect_init(void);

/**
 * @brief 反初始化 pinconnect 硬體
 */
static void system_pinconnect_deinit(void);

/**
 * @brief 初始化 uart3 硬體
 *
 * 鮑率為 38400
 */
static void system_uart3_init(void);

/**
 * @brief 反初始化 uart3 硬體
 */
static void system_uart3_deinit(void);

/**
 * @brief 初始化 NVIC
 *
 */
static void system_NVIC_init(void);

/**
 * @brief 初始化 stdio buffer
 *
 * 會將 stdout, stdin 暫存大小皆設置為 0。
 * 讓 printf, scanf 在每個 byte 操作都呼叫底層函示實現，
 * 而非預設的偵測到'\n'才輸出。
 */
static void stdio_buf_init(void);

/**************************************************************
 * public function implement                                  *
 **************************************************************/
/**
 * @brief 初始化系統
 *
 * 1. clock
 * 2. power
 * 3. pinconnect
 * 4. uart3 for comu
 * 5. ssp1 for peripherals IC
 * 6. systick
 * 7. NVIC
 * 8. stdio buf
 */
void system_init(void) {
    system_clock_init();
    system_power_init();
    system_pinconnect_init();
    system_uart3_init();
    syst_init();
    system_NVIC_init();
    stdio_buf_init();
}

/**
 * @brief 反初始化系統
 */
void system_deinit(void) {
    system_uart3_deinit();
    system_pinconnect_deinit();
    system_power_deinit();
    system_clock_deinit();
}

void system_delay_ms(uint16_t ms) {
    uint16_t delay;
    volatile uint32_t i;
    for (delay = ms; delay > 0; delay--) {
        // 1 ms loop with -Os optimisation
        for (i = 8750; i > 0; i--) {
            ;
        }
    }
}

/**************************************************************
 * static function implement                                  *
 **************************************************************/
static void system_clock_init(void) {
    /**
     * clk_pll0 = (2 *  M * sys_clk) / N
     *          = (2 * 20 *    12e6) / 1
     *          = 480e6
     *          = 480 MHz
     *
     * M should be 6~512
     * N should be 1~32
     *
     * clk_cpu = clk_pll0 / Devider
     *         = 480 MHz  / 4
     *         = 120 MHz
     */
    // enable main oscillator
    SCS = SCS_OSCEN;

    // select osc_clk as sys_clk
    CLKSRCSEL = CLKSRCSEL_CLKSRC_OSC;

    // Wait for Oscillator to be ready
    while ((SCS & SCS_OSCSTAT) == 0) {
        ;
    }

    // set PLL0 multiplier M and divider N
    PLL0CFG = ((PLL0_M - 1) << PLL0CFG_MSEL_POS) |
              ((PLL0_N - 1) << PLL0CFG_NSEL_POS);
    PLL0FEED = 0xAA;
    PLL0FEED = 0x55;

    // enable and connect PLL0
    PLL0CON = PLL0CON_ENABLE | PLL0CON_CONNECT;
    PLL0FEED = 0xAA;
    PLL0FEED = 0x55;

    // Wait for PLL0 locked
    while (!(PLL0STAT & PLL0STAT_LOCK)) {
        ;
    }

    // Wait for PLL0 enabled and connected
    while ((PLL0STAT & (PLL0STAT_ENABLE | PLL0STAT_CONNECT)) !=
           (PLL0STAT_ENABLE | PLL0STAT_CONNECT)) {
        ;
    }

    CCLKCFG = CPU_CLK_DEVIDER - 1;
}

static void system_clock_deinit(void) {
    PLL0CON = 0;
    PLL0FEED = 0xAA;
    PLL0FEED = 0x55;

    PLL0CFG = 0;
    PLL0FEED = 0xAA;
    PLL0FEED = 0x55;

    SCS = 0;
    CLKSRCSEL = 0;

    CCLKCFG = 0;
    PCLKSEL0 = 0;
    PCLKSEL1 = 0;
}

static void system_power_init(void) {
    PCONP = PCONP_UART3 | PCONP_GPIO | PCONP_ENET;
}

static void system_power_deinit(void) {
    PCONP = 0;
}

static void system_pinconnect_init(void) {
    // set P0.0 and P0.1 as TXD3 and RXD3
    PINSEL0 = (PIN_FUNC_2 << 0) | (PIN_FUNC_2 << 2);

    // set pins as Ethnet pins
    // P1.0, P1.1, P1.4, P1.8, P1.9, P1.10, P1.14, P1.15
    PINSEL2 = (PIN_FUNC_1 << 0) | (PIN_FUNC_1 << 2) |
              (PIN_FUNC_1 << 8) | (PIN_FUNC_1 << 16) |
              (PIN_FUNC_1 << 18) | (PIN_FUNC_1 << 20) |
              (PIN_FUNC_1 << 28) | (PIN_FUNC_1 << 30);

    // set pins as Ethnet pins
    // P1.16, P1.17
    PINSEL3 = (PIN_FUNC_1 << 0) | (PIN_FUNC_1 << 2);
}

static void system_pinconnect_deinit(void) {
    PINSEL0 = 0;
}

static void system_uart3_init(void) {
    /**
     * pclk_uart3 = cpu_clk / 4
     *            = 120e6   / 4
     *            = 30e6
     *
     * baudrate = pclk / 16 / (256*DLM + DLL) / (1+ DIV/MUL)
     *          = 30e6 / 16 / (256* 0  +  39) / (1+  1 /  4)
     *          = 38461.5385
     *
     * err = (38461.5385-38400)/38400*100
     *     = 0.16%
     */

    // enable uart fifo
    U3FCR = USART_FCR_FIFOEN;

    // get acess for Divisor Latch
    U3LCR = USART_LCR_DLAB;

    // set baudrate
    U3FDR = (MUL_VAL << USART_FDR_MULVAL_POS) |
            (DIVADD_VAL << USART_FDR_DIVADDVAL_POS);
    U3DLL = DLL_VAL;
    U3DLM = DLM_VAL;

    // set character length as 8-bit
    // clr access for Divisor Latch
    U3LCR = (3 << USART_LCR_WLS_POS);
}

static void system_uart3_deinit(void) {
    U3FDR = 0x10;
    U3DLL = 0x01;
    U3DLM = 0x00;
    U3LCR = 0x00;
}

static void system_NVIC_init(void) {
    // SHPR PRI_n only bits[7:3] of each field
    // Set intterrupt priority of SysTick to least urgency
    __set_faultn_priority(SysTick_IRQn, 1);
    __enable_irq();
}

static void stdio_buf_init(void) {
    setbuf(stdout, NULL);
    setbuf(stdin, NULL);
}
