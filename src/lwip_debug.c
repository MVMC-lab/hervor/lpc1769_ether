/**
 * @file lwip_debug.c
 * @author LiYu87@mvmc-lab
 * @brief LWIP debug re-direction
 * @date 2020.04.13
 */

#include "lwip/opt.h"

#ifdef LWIP_DEBUG

/* Displays an error message on assertion */
void assert_printf(char *msg, int line, char *file)
{
	if (msg) {
		printf("%s:%d in file %s\n", msg, line, file);
	}
	while (1) {}
}

#else
/* LWIP optimized assertion loop (no LWIP_DEBUG) */
void assert_loop(void)
{
	while (1) {}
}

#endif /* LWIP_DEBUG */

/**
 * @}
 */
