/**
 * @file enet.c
 * @author LiYu87@mvmc-lab
 * @brief Ethernet driver
 * @date 2020.04.07
 *
 */

#include "enet.h"

#include "LPC1769.h"
#include "enet_cfg.h"
#include "enet_reg.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/
EnetTxDesc_t TxDescs[ENET_NUM_TX_DESCS]
    __attribute__((section("AHBSRAM1")));
EnetTxStat_t TxStats[ENET_NUM_TX_DESCS]
    __attribute__((section("AHBSRAM1")));
EnetRxDesc_t RxDescs[ENET_NUM_RX_DESCS]
    __attribute__((section("AHBSRAM1")));
EnetRxStat_t RxStats[ENET_NUM_RX_DESCS]
    __attribute__((section("AHBSRAM1")));

uint8_t TxBuffer[ENET_NUM_TX_DESCS][ENET_ETH_MAX_FLEN]
    __attribute__((section("AHBSRAM1")));

uint8_t RxBuffer[ENET_NUM_RX_DESCS][ENET_ETH_MAX_FLEN]
    __attribute__((section("AHBSRAM1")));

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/
void enet_reset(void) {
    volatile uint32_t i;

    /* This should be called prior to IP_ENET_Init. The MAC controller
       may not be ready for a call to init right away so a small delay
       should occur after this call. */

    MAC1 = ENET_MAC1_RESETTX_MASK | ENET_MAC1_RESETMCSTX_MASK |
           ENET_MAC1_RESETRX_MASK | ENET_MAC1_RESETMCSRX_MASK |
           ENET_MAC1_SIMRESET_MASK | ENET_MAC1_SOFTRESET_MASK;

    Command = ENET_COMMAND_TXRESET_MASK | ENET_COMMAND_RXRESET_MASK |
              ENET_COMMAND_REGRESET_MASK;

    /* NOTE: enet reset need a small delay */
    for (i = 0; i < 100; i++) {
        ;
    }
}

void enet_init(void) {
    enet_reset();

    /* Initial MAC configuration for  full duplex,
       100Mbps, inter-frame gap use default values */
    MAC1 = ENET_MAC1_PARF_MASK;
    MAC2 = ENET_MAC2_FULLDUPLEX_MASK | ENET_MAC2_CRCEN_MASK |
           ENET_MAC2_PADCRCEN_MASK;

    Command = ENET_COMMAND_FULLDUPLEX_MASK |
              ENET_COMMAND_PASSRUNTFRAME_MASK |
              ENET_COMMAND_RMII_MASK;

    IPGT = ENET_IPGT_GAP_FULLDUPLEX_DEF << ENET_IPGT_BTOBINTEGAP_POS;
    IPGR =
        (ENET_IPGR_NBTOBINTEGAP1_DEF << ENET_IPGR_NBTOBINTEGAP1_POS) |
        (ENET_IPGR_NBTOBINTEGAP2_DEF << ENET_IPGR_NBTOBINTEGAP2_POS);
    SUPP = ENET_SUPP_100MBPS_SPEED_MASK;
    CLRT = (ENET_CLRT_COLLWIN_DEF << ENET_CLRT_COLLWIN_POS) |
           (ENET_CLRT_RETRANSMAX_DEF << ENET_CLRT_RETRANSMAX_POS);
    MAXF = ENET_ETH_MAX_FLEN;

    /* Setup rx filter */
    RxFilterCtrl = ENET_RXFILTERCTRL_AUE_MASK |
                   ENET_RXFILTERCTRL_ABE_MASK |
                   ENET_RXFILTERCTRL_APE_MASK;

    /* Clear all MAC interrupts */
    IntClear = 0xFFFF;

    /* Disable MAC interrupts */
    IntEnable = 0;
}

void enet_deinit(void) {
    /* Disable packet reception */
    MAC1 &= ~ENET_MAC1_RXENABLE_MASK;
    Command = 0;

    /* Clear all MAC interrupts */
    IntClear = 0xFFFF;

    /* Disable MAC interrupts */
    IntEnable = 0;
}

inline void enet_tx_enable(void) {
    Command |= ENET_COMMAND_TXENABLE_MASK;
}

inline void enet_tx_disable(void) {
    Command &= ~ENET_COMMAND_TXENABLE_MASK;
}

inline void enet_tx_reset(void) {
    MAC1 |= ENET_MAC1_RESETTX_MASK;
}

inline void enet_rx_enable(void) {
    Command |= ENET_COMMAND_RXENABLE_MASK;
    MAC1 |= ENET_MAC1_RXENABLE_MASK;
}

inline void enet_rx_disable(void) {
    Command &= ~ENET_COMMAND_RXENABLE_MASK;
    MAC1 &= ~ENET_MAC1_RXENABLE_MASK;
}

inline void enet_rx_reset(void) {
    MAC1 |= ENET_MAC1_RESETRX_MASK;
}

inline void enet_set_addr(const uint8_t *mac_p) {
    SA0 = ((uint32_t)mac_p[5] << 8) | ((uint32_t)mac_p[4]);
    SA1 = ((uint32_t)mac_p[3] << 8) | ((uint32_t)mac_p[2]);
    SA2 = ((uint32_t)mac_p[1] << 8) | ((uint32_t)mac_p[0]);
}

/**
 * MII Management Clock (MDC) need a clock.
 *
 * For DP83848, type MDC is 2.5 MHz, and max is 25 MHz.
 *
 * clk_enet = clk_cpu = 120 MHz
 *
 * avaiable devider_MDC N is
 *   4,  6,  8, 10, 14, 20, 28, 36
 *  40, 44, 48, 52, 56, 60, 64
 *
 * clk_MDC = clk_enet / devider_MDC
 *         = 120 MHz  / 48
 *         = 2.5 MHz
 *
 */
void enet_mii_init(void) {
    /* hold reset and set clock */
    MCFG = (ENET_MCFG_CLOCKSEL_DIV48 << ENET_MCFG_CLOCKSEL_POS) |
           ENET_MCFG_RESETMIIMGMT_MASK;

    /* release reset */
    MCFG &= ~ENET_MCFG_RESETMIIMGMT_MASK;
}

uint8_t enet_mii_is_busy(void) {
    if (MIND & ENET_MIND_BUSY_MASK) {
        return 1;
    }
    else {
        return 0;
    }
}

void enet_mii_start_read(uint8_t phy, uint8_t reg) {
    MCMD = 0;
    MADR = ((reg & 0b11111) << ENET_MADR_REGADDR_POS) |
           ((phy & 0b11111) << ENET_MADR_PHYADDR_POS);

    MCMD = ENET_MCMD_READ_MASK;
}

uint16_t enet_mii_read_data(void) {
    MCMD = 0;
    return MRDD & 0xFFFF;
}

uint16_t enet_mii_read_w(uint8_t phy, uint8_t reg) {
    enet_mii_start_read(phy, reg);
    while (enet_mii_is_busy()) {
        ;
    }
    return enet_mii_read_data();
}

void enet_mii_start_write(uint8_t phy, uint8_t reg, uint16_t data) {
    MCMD = 0;
    MADR = ((reg & 0b11111) << ENET_MADR_REGADDR_POS) |
           ((phy & 0b11111) << ENET_MADR_PHYADDR_POS);
    MWTD = data;
}

void enet_mii_write_w(uint8_t phy, uint8_t reg, uint16_t data) {
    enet_mii_start_write(phy, reg, data);
    while (enet_mii_is_busy()) {
        ;
    }
}

void enet_descriptors_init(void) {
    int i;

    /* Build linked list, CPU is owner of descriptors */
    for (i = 0; i < ENET_NUM_RX_DESCS; i++) {
        RxDescs[i].Packet = (uint32_t)RxBuffer[i];
        RxDescs[i].Control = ENET_ETH_MAX_FLEN - 1;
        RxStats[i].StatusInfo = 0;
        RxStats[i].StatusHashCRC = 0;
    }
    for (i = 0; i < ENET_NUM_TX_DESCS; i++) {
        TxDescs[i].Packet = (uint32_t)TxBuffer[i];
        TxDescs[i].Control = 0;
        TxStats[i].StatusInfo = 0;
    }

    RxDescriptorNumber = ENET_NUM_RX_DESCS - 1;
    RxDescriptor = (uint32_t)RxDescs;
    RxStatus = (uint32_t)RxStats;
    RxConsumeIndex = 0;

    TxDescriptorNumber = ENET_NUM_TX_DESCS - 1;
    TxDescriptor = (uint32_t)TxDescs;
    TxStatus = (uint32_t)TxStats;
    TxProduceIndex = 0;
}

void enet_phy_reset(uint8_t phy) {
    enet_mii_write_w(phy, 0x00, _BV(15));
}

void enet_setup_by_physt(uint16_t physt __attribute__((unused))) {
}
