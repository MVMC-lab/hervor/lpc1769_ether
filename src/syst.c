/**
 * @file syst.c
 * @author LiYu87@mvmc-lab
 * @brief Systick driver
 * @date 2020.04.10
 */

#include "syst.h"

#include "LPC1769.h"
#include "syst_reg.h"

/**
 * @brief time unit 0.0001s
 *
 */
static volatile uint32_t systick_val = 0;

void SysTick_Handler(void) {
    systick_val++;
}

uint32_t syst_get(void) {
    return systick_val;
}

void syst_init(void) {
    /**
     * clk_systick = cpu_clk / (SYST_RVR + 1)
     *             = 120e6   / (11999    + 1)
     *             = 120e6   / 12e2
     *             = 1e4 Hz (0.1 ms)
     */
    SYST_RVR = 11999;

    SYST_CVR = 0;

    /* enanle systick, interrupt and select  processor clock. */
    SYST_CSR = SYST_CSR_ENABLE_MASK | SYST_CSR_TICKINT_MASK |
               SYST_CSR_CLKSOURCE_MASK;
}
