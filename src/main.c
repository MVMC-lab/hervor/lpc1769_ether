
#include <LPC1769.h>
#include <stdio.h>

#include "delay.h"
#include "echo.h"
#include "enet.h"
#include "ible.h"
#include "lwip/dhcp.h"
#include "lwip/init.h"
#include "lwip/ip_addr.h"
#include "lwip/netif.h"
#include "lwip/opt.h"
#include "lwip/sys.h"
#include "lwip/timeouts.h"
#include "netif/etharp.h"
#include "system.h"
#include "uart3.h"

static struct netif netif;

int main(void) {
    system_init();
    printf("hello world=======================================\n");

    lwip_init();

    ip_addr_t ipaddr, netmask, gw;

#if LWIP_DHCP
    IP4_ADDR(&gw, 0, 0, 0, 0);
    IP4_ADDR(&ipaddr, 0, 0, 0, 0);
    IP4_ADDR(&netmask, 0, 0, 0, 0);
#else
    IP4_ADDR(&gw, 10, 1, 10, 1);
    IP4_ADDR(&ipaddr, 10, 1, 10, 234);
    IP4_ADDR(&netmask, 255, 255, 255, 0);
    APP_PRINT_IP(&ipaddr);
#endif

    netif_add(&netif, &ipaddr, &netmask, &gw, NULL, ible_netif_init,
              ethernet_input);
    netif_set_default(&netif);
    netif_set_link_up(&netif);
    netif_set_up(&netif);

#if LWIP_DHCP
    dhcp_start(&netif);
#endif

    uint8_t prt_ip = 0;
    echo_init();

    while (1) {
        ible_input(&netif);
        sys_check_timeouts();
        /* Print IP address info */
        if (!prt_ip) {
            if (netif.ip_addr.addr) {
                static char tmp_buff[16];
                printf("IP_ADDR    : %s\r\n",
                       ipaddr_ntoa_r((const ip_addr_t *)&netif.ip_addr,
                                     tmp_buff, 16));
                printf("NET_MASK   : %s\r\n",
                       ipaddr_ntoa_r((const ip_addr_t *)&netif.netmask,
                                     tmp_buff, 16));
                printf(
                    "GATEWAY_IP : %s\r\n",
                    ipaddr_ntoa_r((const ip_addr_t *)&netif.gw, tmp_buff, 16));
                printf("sys_now : %ld\r\n", sys_now());
                prt_ip = 1;
            }
        }
    }

    return 0;
}
