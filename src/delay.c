/**
 * @file delay.c
 * @author LiYu87@mvmc-lab
 * @brief 實現延遲函式
 * @date 2020.04.14
 */

#include "delay.h"
#include "syst.h"

void delay_ms(uint16_t ms) {
    volatile uint32_t t = syst_get();
    while (1) {
        if (syst_get() - t > ms * 10) {
            break;
        }
    }
}
