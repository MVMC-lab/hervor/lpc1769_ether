/**
 * @file lwip_sys_now.c
 * @author LiYu87@mvmc-lab
 * @brief Provide sys_now function for lwip.
 * @date 2020.04.13
 */

#include "syst.h"

uint32_t sys_now(void) {
    return syst_get() / 10;
}
