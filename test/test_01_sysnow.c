/**
 * @file test_01_sysnow.c
 * @author LiYu87@mvmc-lab
 * @brief sysnow function and systick test.
 * @date 2020.04.14
 */

#include <stdio.h>

#include "delay.h"
#include "syst.h"
#include "system.h"

int main() {
    system_init();
    printf("test_01_sysnow\n");

    uint32_t cur_time = 0;

    while (1) {
        cur_time = syst_get();
        printf("current systick is %ld\n", cur_time);
        delay_ms(1000);
    }

    return 0;
}
