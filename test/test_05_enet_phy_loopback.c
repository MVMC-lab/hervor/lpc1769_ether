/**
 * @file test_05_enet_phy_loopback.c
 * @author LiYu87@mvmc-lab
 * @brief 使用 PHY 硬體 loopback 功能測試frame送收
 * @date 2020.04.21
 *
 * 硬體連線：
 *           RMII
 * LPC1769 <------> DP83848
 *
 * FIXME: 現在此測試無法正常運行
 * TX資料不會經由 DP83848 傳送回 RX
 */

#include <stdio.h>

#include "delay.h"
#include "enet.h"
#include "system.h"

/**
 * default id for DP83848: 0x01
 */
#define PHY_ID 0x01

void tx_send_dummy(void);
uint8_t is_rx_have_data();
void rx_get(void);

int main() {
    system_init();

    printf("test_05_enet_phy_loopback\n");

    enet_init();

    enet_mii_init();

    // 寫入 BCR bit 0 重置 DP83848
    enet_phy_reset(PHY_ID);

    // 啟用 PHY 的 LOOPBACK
    enet_mii_write_w(PHY_ID, PHY_BCR, PHY_BCR_LOOPBACK_MASK);

    enet_descriptors_init();

    enet_tx_enable();
    enet_rx_enable();

    tx_send_dummy();
    tx_send_dummy();

    while(1) {
        printf("in main loop\n");
        if (is_rx_have_data()) {
            rx_get();
        }
        delay_ms(1000);
    }

    return 0;
}

void tx_send_dummy(void) {
    uint32_t tx_idx = TxProduceIndex;
    EnetTxDesc_t *txd_p = (void*)TxDescriptor;
    uint8_t *txbuffer_p = (void*)txd_p[tx_idx].Packet;

    txd_p[tx_idx].Control = 0;

    /**
     * Destination (6 bytes)
     * use boardcast MAC FF:FF:FF:FF:FF:FF
     */
    txbuffer_p[0] = 0xFF;
    txbuffer_p[1] = 0xFF;
    txbuffer_p[2] = 0xFF;
    txbuffer_p[3] = 0xFF;
    txbuffer_p[4] = 0xFF;
    txbuffer_p[5] = 0xFF;

    /**
     * Source (6 bytes)
     * use MAC save in SA registers
     */
    txbuffer_p[6]  = (SA0 & 0xFF00) >> 8;
    txbuffer_p[7]  = (SA0 & 0x00FF) >> 0;
    txbuffer_p[8]  = (SA1 & 0xFF00) >> 8;
    txbuffer_p[9]  = (SA1 & 0x00FF) >> 0;
    txbuffer_p[10] = (SA2 & 0xFF00) >> 8;
    txbuffer_p[11] = (SA2 & 0x00FF) >> 0;

    /**
     * Length (2 bytes)
     * payload is 256 bytes
     */
    txbuffer_p[12] = 0;
    txbuffer_p[13] = 0xFF;

    /**
     * Payload (N bytes, N is Length)
     */
    for (uint16_t i = 0; i < 256; i++) {
        txbuffer_p[14 + i] = (i & 0xFF);
    }

    /* -1 for -1 encoded */
    txd_p[tx_idx].Control =
        ((6 + 6 + 2 + 256) - 1) << ENET_TCTRL_SIZE_POS |
        ENET_TCTRL_LAST_MASK;

    TxProduceIndex = (tx_idx + 1) % ENET_NUM_TX_DESCS;
}

uint8_t is_rx_have_data() {
    return (RxProduceIndex != RxConsumeIndex) ? 1 : 0;
}

void rx_get(void) {
    uint32_t rx_idx = RxConsumeIndex;
    EnetRxDesc_t *rxd_p = (void*)RxDescriptor;
    EnetRxStat_t *rxs_p = (void*)RxStatus;
    uint8_t *rxbuffer_p = (void*)rxd_p[rx_idx].Packet;

    uint16_t fream_len = (rxs_p[rx_idx].StatusInfo & ENET_RINFO_SIZE_MASK) + 1;

    printf("frame length is %d\n", fream_len);

    printf("frame data:\n");
    for (uint16_t i = 0; i < fream_len; i++) {
        printf("%02X ", rxbuffer_p[i]);
    }
    printf("\n");

    RxConsumeIndex = (rx_idx + 1) % ENET_NUM_RX_DESCS;
}
