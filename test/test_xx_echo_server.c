/**
 * @file test_xx_echo_server.c
 * @author LiYu87@mvmc-lab
 * @brief TCP echo server example using raw API.
 * @date 2020.04.24
 */

#include <LPC1769.h>
#include <stdio.h>

#include "delay.h"
#include "enet.h"
#include "ible.h"
#include "lwip/init.h"
#include "lwip/ip_addr.h"
#include "lwip/memp.h"
#include "lwip/netif.h"
#include "lwip/opt.h"
#include "lwip/sys.h"
#include "lwip/tcp.h"
#include "lwip/tcpip.h"
#include "netif/etharp.h"
#include "system.h"
#include "uart3.h"

static struct netif netif;

void stdio_init(void);

int main(void) {
    system_init();
    printf("test_xx_echo_server\n");

    lwip_init();

    ip_addr_t ipaddr, netmask, gw;

    IP4_ADDR(&gw, 0, 0, 0, 0);
    IP4_ADDR(&ipaddr, 0, 0, 0, 0);
    IP4_ADDR(&netmask, 0, 0, 0, 0);

    // initialize and setup netif
    netif_add(&netif, &ipaddr, &netmask, &gw, NULL, ible_netif_init,
              ethernet_input);
    netif_set_default(&netif);
    netif_set_link_up(&netif);
    netif_set_up(&netif);

    // use DHCP to request IP
    dhcp_start(&netif);

    uint8_t prt_ip = 0;

    // start echo server
    echo_init();

    while (1) {
        ible_input(&netif);
        sys_check_timeouts();

        /* Print IP address info once when it setup */
        if (!prt_ip) {
            if (netif.ip_addr.addr) {
                static char tmp_buff[16];
                printf(
                    "IP_ADDR    : %s\r\n",
                    ipaddr_ntoa_r((const ip_addr_t *)&netif.ip_addr, tmp_buff, 16)
                );
                printf(
                    "NET_MASK   : %s\r\n",
                    ipaddr_ntoa_r((const ip_addr_t *)&netif.netmask, tmp_buff, 16)
                );
                printf(
                    "GATEWAY_IP : %s\r\n",
                    ipaddr_ntoa_r((const ip_addr_t *)&netif.gw, tmp_buff, 16)
                );
                printf("sys_now : %ld\r\n", sys_now());
                prt_ip = 1;
            }
        }
    }

    return 0;
}
