/**
 * @file test_06_enet_pass_rxfilter.c
 * @author LiYu87@mvmc-lab
 * @brief bypass RX 過濾器，接收所有frame
 * @date 2020.04.21
 *
 * 硬體連線：
 *           RMII
 * LPC1769 <------> DP83848
 */

#include <stdio.h>

#include "delay.h"
#include "enet.h"
#include "system.h"

/**
 * default id for DP83848: 0x01
 */
#define PHY_ID 0x01

uint8_t is_rx_have_data();
void rx_get(void);

int main() {
    system_init();

    printf("test_06_enet_pass_rxfilter\n");

    enet_init();

    // Bypass RX 過濾器
    Command |= ENET_COMMAND_PASSRXFILTER_MASK;

    enet_mii_init();

    // 寫入 BCR bit 0 重置 DP83848
    enet_phy_reset(PHY_ID);

    enet_descriptors_init();

    enet_tx_enable();
    enet_rx_enable();

    while(1) {
        printf("in main loop\n");
        if (is_rx_have_data()) {
            rx_get();
        }
        delay_ms(500);
    }

    return 0;
}

uint8_t is_rx_have_data() {
    return (RxProduceIndex != RxConsumeIndex) ? 1 : 0;
}

void rx_get(void) {
    uint32_t rx_idx = RxConsumeIndex;
    EnetRxDesc_t *rxd_p = (void*)RxDescriptor;
    EnetRxStat_t *rxs_p = (void*)RxStatus;
    uint8_t *rxbuffer_p = (void*)rxd_p[rx_idx].Packet;

    uint16_t fream_len = (rxs_p[rx_idx].StatusInfo & ENET_RINFO_SIZE_MASK) + 1;

    printf("frame length is %d\n", fream_len);

    printf("frame data:\n");
    for (uint16_t i = 0; i < fream_len; i++) {
        printf("%02X ", rxbuffer_p[i]);
    }
    printf("\n");

    RxConsumeIndex = (rx_idx + 1) % ENET_NUM_RX_DESCS;
}
