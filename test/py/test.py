import socket
import time

HOST = '192.168.1.85'
PORT = 7

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.settimeout(0.1)
s.connect(("192.168.1.85", PORT))

time.sleep(1)

while True:
    time.sleep(1)

    serverMessage = 'I\'m here!'
    s.send(serverMessage.encode())

    clientMessage = str(s.recv(1024), encoding='utf-8')

    print('Server message is:', clientMessage)
