/**
 * @file test_03_enet_phy_print_reg.c
 * @author LiYu87@mvmc-lab
 * @brief MII and PHY module test.
 * @date 2020.04.14
 *
 * 硬體連線：
 *           RMII
 * LPC1769 <------> DP83848
 */

#include <stdio.h>

#include "delay.h"
#include "enet.h"
#include "system.h"

/**
 * default id for DP83848: 0x01
 */
#define PHY_ID 0x01

void print_all_reg(void);

int main() {
    system_init();
    printf("test_03_enet_phy_print_reg\n");

    enet_init();
    enet_mii_init();

    // 寫入 BCR bit 0 重置 DP83848
    enet_phy_reset(PHY_ID);

    print_all_reg();

    return 0;
}

void print_all_reg(void) {
    uint16_t data;

    // Basic registers
    printf("\n");
    printf("Basic registers:\n");
    data = enet_mii_read_w(PHY_ID, PHY_BCR);
    printf("    BCR(%02Xh): 0x%04X\n", PHY_BCR, data);
    data = enet_mii_read_w(PHY_ID, PHY_BSR);
    printf("    BSR(%02Xh): 0x%04X\n", PHY_BSR, data);
    data = enet_mii_read_w(PHY_ID, PHY_PHYID1);
    printf(" PHYID1(%02Xh): 0x%04X\n", PHY_PHYID1, data);
    data = enet_mii_read_w(PHY_ID, PHY_PHYID2);
    printf(" PHYID2(%02Xh): 0x%04X\n", PHY_PHYID2, data);
    data = enet_mii_read_w(PHY_ID, PHY_ANAR);
    printf("   ANAR(%02Xh): 0x%04X\n", PHY_ANAR, data);
    data = enet_mii_read_w(PHY_ID, PHY_ANLPAR);
    printf(" ANLPAR(%02Xh): 0x%04X\n", PHY_ANLPAR, data);
    data = enet_mii_read_w(PHY_ID, PHY_ANER);
    printf("   ANER(%02Xh): 0x%04X\n", PHY_ANER, data);
    data = enet_mii_read_w(PHY_ID, PHY_ANNPTR);
    printf(" ANNPTR(%02Xh): 0x%04X\n", PHY_ANNPTR, data);

    // Extend registers
    printf("\n");
    printf("Extend registers:\n");
    data = enet_mii_read_w(PHY_ID, PHY_DP8_PHYSTS);
    printf(" PHYSTS(%02Xh): 0x%04X\n", PHY_DP8_PHYSTS, data);
    data = enet_mii_read_w(PHY_ID, PHY_DP8_MICR);
    printf("   MICR(%02Xh): 0x%04X\n", PHY_DP8_MICR, data);
    data = enet_mii_read_w(PHY_ID, PHY_DP8_MISR);
    printf("   MISR(%02Xh): 0x%04X\n", PHY_DP8_MISR, data);
    data = enet_mii_read_w(PHY_ID, PHY_DP8_FCSCR);
    printf("  FCSCR(%02Xh): 0x%04X\n", PHY_DP8_FCSCR, data);
    data = enet_mii_read_w(PHY_ID, PHY_DP8_RECR);
    printf("   RECR(%02Xh): 0x%04X\n", PHY_DP8_RECR, data);
    data = enet_mii_read_w(PHY_ID, PHY_DP8_PCSR);
    printf("   PCSR(%02Xh): 0x%04X\n", PHY_DP8_PCSR, data);
    data = enet_mii_read_w(PHY_ID, PHY_DP8_RBR);
    printf("    RBR(%02Xh): 0x%04X\n", PHY_DP8_RBR, data);
    data = enet_mii_read_w(PHY_ID, PHY_DP8_LEDCR);
    printf("  LEDCR(%02Xh): 0x%04X\n", PHY_DP8_LEDCR, data);
    data = enet_mii_read_w(PHY_ID, PHY_DP8_PHYCR);
    printf("  PHYCR(%02Xh): 0x%04X\n", PHY_DP8_PHYCR, data);
    data = enet_mii_read_w(PHY_ID, PHY_DP8_10BT_SERIAL);
    printf("10BT_SERIAL(%02Xh): 0x%04X\n", PHY_DP8_10BT_SERIAL,
           data);
    data = enet_mii_read_w(PHY_ID, PHY_DP8_CDCTRL1);
    printf("CDCTRL1(%02Xh): 0x%04X\n", PHY_DP8_CDCTRL1, data);
    data = enet_mii_read_w(PHY_ID, PHY_DP8_EDCR);
    printf("   EDCR(%02Xh): 0x%04X\n", PHY_DP8_EDCR, data);
}
