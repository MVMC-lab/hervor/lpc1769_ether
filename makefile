#-----------------------------------------------------------------#
# Basic Settings                                                  #
#-----------------------------------------------------------------#
## Target name
TARGET = main

## Hardware info
BOARD  = asa_m3_v1
ARCH   = cortex-m3
MCU    = LPC1769

## Source files and search directories
CSRC	= uart3.c system.c syscall.c delay.c syst.c enet.c ible.c echo.c
CSRC   += lwip_sys_now.c lwip_debug.c
CSRC   += startup_LPC1769.c
CSRC   += $(notdir $(LWIP_SOURCE))
ASRC	=

## Search path
VPATH     = src
VPATH    += test
VPATH    += lib/$(MCU)_startup
VPATH    += $(LWIP_VPATH)

## Include path
INC_PATH  = inc
INC_PATH += inc/lwip
INC_PATH += lib/$(MCU)_startup
INC_PATH += $(LWIP_INCDIR)

## Programs to build porject
CC      = arm-none-eabi-gcc
OBJCOPY = arm-none-eabi-objcopy
OBJDUMP = arm-none-eabi-objdump
SIZE    = arm-none-eabi-size
NM      = arm-none-eabi-nm

## Path to output obj files (eg: .o, .d)
OBJDIR = obj

#-----------------------------------------------------------------#
# LWIP Settings                                                   #
#-----------------------------------------------------------------#
LWIPDIR = lib/lwip/src
include lib/lwip/src/Filelists.mk

# LWIPNOAPPSFILES: All LWIP files without apps
LWIP_SOURCE  = $(LWIPNOAPPSFILES)

LWIP_VPATH   = $(LWIPDIR)/core
LWIP_VPATH  += $(LWIPDIR)/core/ipv4
LWIP_VPATH  += $(LWIPDIR)/core/ipv6
LWIP_VPATH  += $(LWIPDIR)/api
LWIP_VPATH  += $(LWIPDIR)/netif
LWIP_VPATH  += $(LWIPDIR)/netif/ppp
LWIP_VPATH  += $(LWIPDIR)/netif/ppp/polarssl

LWIP_INCDIR = lib/lwip/src/include

#-----------------------------------------------------------------#
# Compiler Settings                                               #
#-----------------------------------------------------------------#
## Optimization level (0, 1, 2, 3, 4 or s)
OPTIMIZE = s
DEBUG    = dwarf-2

## C Standard level (c89, gnu89, c99 or gnu99)
CSTD     = c99

## Macros for preprocessor
DEFS     = $(shell echo $(BOARD) | tr '[:lower:]' '[:upper:]')

# Object files for linking
COBJ     = $(addprefix $(OBJDIR)/, $(CSRC:.c=.o))
AOBJ     = $(addprefix $(OBJDIR)/, $(ASRC:.S=.o))

## Link script
LINKSCRIPT = lib/$(MCU)_startup/$(MCU).ld

#-----------------------------------------------------------------#
# Flags for C compiler                                            #
#-----------------------------------------------------------------#
## Hardware Info
CFLAGS  = -mcpu=$(ARCH)
## Optimization
CFLAGS += -Os
CFLAGS += -ffunction-sections -fdata-sections
## Thumb or ARM instruction settings
CFLAGS += -mthumb
CFLAGS += -mthumb-interwork
## Else
CFLAGS += $(addprefix -I,$(INC_PATH))
CFLAGS += $(addprefix -D,$(DEFS))
CFLAGS += -std=$(CSTD)
CFLAGS += -g$(DEBUG)
CFLAGS += -Wall -Wextra
CFLAGS += -Wp,-MM,-MP,-MT,$(OBJDIR)/$(*F).o,-MF,$(OBJDIR)/$(*F).d

#-----------------------------------------------------------------#
# Flags for Assembler                                             #
#-----------------------------------------------------------------#
# Hardware Info
ASFLAGS  = -mcpu=$(MCU)
## Thumb or ARM instruction settings
ASFLAGS += -mthumb
ASFLAGS += -mthumb-interwork
## Else
ASFLAGS += -x assembler-with-cpp
ASFLAGS += -Wa,-adhlns=$(<:.S=.lst),-gstabs
ASFLAGS += $(addprefix -D,$(DEFS))

#-----------------------------------------------------------------#
# Flags for Linker                                                #
#-----------------------------------------------------------------#
LDFLAGS  = -Wl,-Map,$(TARGET).map,--cref
LDFLAGS += -Wl,--gc-sections
LDFLAGS += -nostartfiles
## Thumb or ARM instruction settings
LDFLAGS += -mthumb
LDFLAGS += -mthumb-interwork
LDFLAGS += -T$(LINKSCRIPT)
## Linking library
LDFLAGS += -lc
LDFLAGS += -lgcc
# LDFLAGS += -lm
LDFLAGS += -specs=nosys.specs --specs=nano.specs -flto

all: $(TARGET).hex

#-----------------------------------------------------------------#
# Command for update the lib                                      #
#-----------------------------------------------------------------#
LIB_DOWNLOAD_URL = https://gitlab.com/MVMC-lab/arm_gcc_startup/-/archive/master/arm_gcc_startup-master.zip
LIB_EXTRACT = arm_gcc_startup-master/platform/LPC1769
update-lib:
	curl -o lib.zip $(LIB_DOWNLOAD_URL)
	-rm -r lib/$(MCU)_startup
	-mkdir lib
	-mkdir lib/$(MCU)_startup
	tar -xm -f lib.zip -C lib
	mv lib/$(LIB_EXTRACT)/* lib/$(MCU)_startup
	rm -r lib/arm_gcc_startup-master
	rm lib.zip

#-----------------------------------------------------------------#
# Download Command (jlink, py-asa-loader)                         #
#-----------------------------------------------------------------#
JLINK_OPT  = -device $(MCU)
JLINK_OPT += -if SWD
JLINK_OPT += -speed 4000
JLINK_OPT += -autoconnect 1

DOT_JLINK_FILE = JLinkCommandFile.jlink
DOT_JLINK_TEXT  = "r\n"
DOT_JLINK_TEXT += "loadfile $(TARGET).hex\n"
DOT_JLINK_TEXT += "rx 20\n"
DOT_JLINK_TEXT += "exit\n"


download:
	echo -e $(DOT_JLINK_TEXT) > $(DOT_JLINK_FILE)
	Jlink $(JLINK_OPT) -CommanderScript $(DOT_JLINK_FILE)

download_2:
	asaloader prog -p COM7 -f main.hex -D 1500 -a
	terminal -p 7

download_%:
	echo -e "r\n loadfile $(*).hex\n rx 20\n exit\n" > $(DOT_JLINK_FILE)
	Jlink $(JLINK_OPT) -CommanderScript $(DOT_JLINK_FILE)

hold-reset:
	echo -e "r0 \n exit\n" > $(DOT_JLINK_FILE)
	Jlink $(JLINK_OPT) -CommanderScript $(DOT_JLINK_FILE)

reset:
	echo -e "r \n exit\n" > $(DOT_JLINK_FILE)
	Jlink $(JLINK_OPT) -CommanderScript $(DOT_JLINK_FILE)

#-----------------------------------------------------------------#
# test files                                                      #
#-----------------------------------------------------------------#
TESTSRCS  = $(notdir $(wildcard test/*.c))
TESTHEXES = $(patsubst %.c,%.hex, $(TESTSRCS))

test: $(TESTHEXES)

hex: $(TARGET).hex
lst: $(TARGET).lst
elf: $(TARGET).elf

# Display compiler version information.
version :
	@$(CC) --version

%.hex: %.elf
	@echo
	@echo $@ :
	$(OBJCOPY) -O ihex $< $@
	$(SIZE) -x -A $@

%.lst: %.elf
	@echo
	@echo $@ :
	$(OBJDUMP) -h -S -C $< > $@

%.sym: %.elf
	@echo
	@echo $@ :
	$(NM) -n $< > $@

size: %.elf
	$(SIZE) -A $(TARGET).elf

%.elf: $(addprefix $(OBJDIR)/, %.o) $(AOBJARM) $(AOBJ) $(COBJARM) $(COBJ)
	@echo
	@echo $@ :
	$(CC) $(CFLAGS) $(AOBJ) $(COBJ) $< -o $@ $(LDFLAGS)
	$(SIZE) -x -A $@

# Compile: create object files from C source files.
$(OBJDIR)/%.o: %.c
	@echo
	@echo $@ :
	$(CC) -c $(CFLAGS) $< -o $@

# Compile: create object files from C source files.
$(COBJ): $(OBJDIR)/%.o: %.c
	@echo
	@echo $@ :
	$(CC) -c $(CFLAGS) $< -o $@

# Assemble: create object files from assembler source files.
$(AOBJ): $(OBJDIR)/%.o: %.S
	@echo
	@echo $@ :
	$(CC) -c $(AFLAGS) $< -o $@

# Target: clean project.
clean:
	@echo
	rm -f -r $(OBJDIR) $(TARGET).elf $(TARGET).map $(TARGET).lst $(TARGET).map $(DOT_JLINK_FILE) | exit 0

#test any var with make print-XXX
print-%: ; @echo $* = $($*)

# Include the dependency files.
$(shell mkdir obj 2>/dev/null)

-include $(shell mkdir $(OBJDIR) 2>/dev/null) $(wildcard $(OBJDIR)/*.d)

