/**
 * @file delay.h
 * @author LiYu87@mvmc-lab
 * @brief Provide delay functions
 * @date 2020.04.14
 */

#ifndef DELAY_H
#define DELAY_H

#include <inttypes.h>

/**
 * @brief delay in ms (systick + polling)
 *
 * @param ms time in ms
 */
void delay_ms(uint16_t ms);

#endif  /* DELAY_H */
