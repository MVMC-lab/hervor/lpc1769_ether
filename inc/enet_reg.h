/**
 * @file enet_reg.h
 * @author LiYu87@mvmc-lab
 * @brief Ethernet register bit definition
 * @date 2020.04.07
 *
 * "Chapter 10: LPC176x/5x Ethernet" in "UM10360"
 */

#ifndef ENET_REG_H
#define ENET_REG_H

#include <inttypes.h>

#include "LPC1769.h"

/* MAC1 Register bit definition */
#define ENET_MAC1_RXENABLE_POS          0   /*!< Receive Enable */
#define ENET_MAC1_PARF_POS              1   /*!< Pass All Receive Frames */
#define ENET_MAC1_RXFLOWCTRL_POS        2   /*!< RX Flow Control */
#define ENET_MAC1_TXFLOWCTRL_POS        3   /*!< TX Flow Control */
#define ENET_MAC1_LOOPBACK_POS          4   /*!< Loop Back Mode */
#define ENET_MAC1_RESETTX_POS           8   /*!< Reset TX Logic */
#define ENET_MAC1_RESETMCSTX_POS        9   /*!< Reset MAC TX Control Sublayer */
#define ENET_MAC1_RESETRX_POS           10  /*!< Reset RX Logic */
#define ENET_MAC1_RESETMCSRX_POS        11  /*!< Reset MAC RX Control Sublayer */
#define ENET_MAC1_SIMRESET_POS          14  /*!< Simulation Reset */
#define ENET_MAC1_SOFTRESET_POS         15  /*!< Soft Reset MAC */

#define ENET_MAC1_RXENABLE_MASK         (1 << ENET_MAC1_RXENABLE_POS)
#define ENET_MAC1_PARF_MASK             (1 << ENET_MAC1_PARF_POS)
#define ENET_MAC1_RXFLOWCTRL_MASK       (1 << ENET_MAC1_RXFLOWCTRL_POS)
#define ENET_MAC1_TXFLOWCTRL_MASK       (1 << ENET_MAC1_TXFLOWCTRL_POS)
#define ENET_MAC1_LOOPBACK_MASK         (1 << ENET_MAC1_LOOPBACK_POS)
#define ENET_MAC1_RESETTX_MASK          (1 << ENET_MAC1_RESETTX_POS)
#define ENET_MAC1_RESETMCSTX_MASK       (1 << ENET_MAC1_RESETMCSTX_POS)
#define ENET_MAC1_RESETRX_MASK          (1 << ENET_MAC1_RESETRX_POS)
#define ENET_MAC1_RESETMCSRX_MASK       (1 << ENET_MAC1_RESETMCSRX_POS)
#define ENET_MAC1_SIMRESET_MASK         (1 << ENET_MAC1_SIMRESET_POS)
#define ENET_MAC1_SOFTRESET_MASK        (1 << ENET_MAC1_SOFTRESET_POS)

/* MAC2 Register bit definition */
#define ENET_MAC2_FULLDUPLEX_POS        0   /*!< Full-Duplex Mode */
#define ENET_MAC2_FLC_POS               1   /*!< Frame Length Checking */
#define ENET_MAC2_HFEN_POS              2   /*!< Huge Frame Enable */
#define ENET_MAC2_DELAYEDCRC_POS        3   /*!< Delayed CRC Mode */
#define ENET_MAC2_CRCEN_POS             4   /*!< Append CRC to every Frame */
#define ENET_MAC2_PADCRCEN_POS          5   /*!< Pad all Short Frames */
#define ENET_MAC2_VLANPADEN_POS         6   /*!< VLAN Pad Enable */
#define ENET_MAC2_AUTODETPADEN_POS      7   /*!< Auto Detect Pad Enable */
#define ENET_MAC2_PPENF_POS             8   /*!< Pure Preamble Enforcement */
#define ENET_MAC2_LPENF_POS             9   /*!< Long Preamble Enforcement */
#define ENET_MAC2_NOBACKOFF_POS         12  /*!< No Backoff Algorithm */
#define ENET_MAC2_BP_NOBACKOFF_POS      13  /*!< Backoff Presurre / No Backoff */
#define ENET_MAC2_EXCESSDEFER_POS       14  /*!< Excess Defer */

#define ENET_MAC2_FULLDUPLEX_MASK       (1 << ENET_MAC2_FULLDUPLEX_POS)
#define ENET_MAC2_FLC_MASK              (1 << ENET_MAC2_FLC_POS)
#define ENET_MAC2_HFEN_MASK             (1 << ENET_MAC2_HFEN_POS)
#define ENET_MAC2_DELAYEDCRC_MASK       (1 << ENET_MAC2_DELAYEDCRC_POS)
#define ENET_MAC2_CRCEN_MASK            (1 << ENET_MAC2_CRCEN_POS)
#define ENET_MAC2_PADCRCEN_MASK         (1 << ENET_MAC2_PADCRCEN_POS)
#define ENET_MAC2_VLANPADEN_MASK        (1 << ENET_MAC2_VLANPADEN_POS)
#define ENET_MAC2_AUTODETPADEN_MASK     (1 << ENET_MAC2_AUTODETPADEN_POS)
#define ENET_MAC2_PPENF_MASK            (1 << ENET_MAC2_PPENF_POS)
#define ENET_MAC2_LPENF_MASK            (1 << ENET_MAC2_LPENF_POS)
#define ENET_MAC2_NOBACKOFF_MASK        (1 << ENET_MAC2_NOBACKOFF_POS)
#define ENET_MAC2_BP_NOBACKOFF_MASK     (1 << ENET_MAC2_BP_NOBACKOFF_POS)
#define ENET_MAC2_EXCESSDEFER_MASK      (1 << ENET_MAC2_EXCESSDEFER_POS)

/* IPGT Register bit definition */
#define ENET_IPGT_BTOBINTEGAP_POS       0
#define ENET_IPGT_BTOBINTEGAP_MASK      (0x7F << ENET_IPGT_BTOBINTEGAP_POS)

/* Recommended value */
#define ENET_IPGT_GAP_FULLDUPLEX_DEF    0x15
#define ENET_IPGT_GAP_HALFDUPLEX_DEF    0x12

/* IPGR Register bit definition */
#define ENET_IPGR_NBTOBINTEGAP2_POS     0
#define ENET_IPGR_NBTOBINTEGAP1_POS     8

#define ENET_IPGR_NBTOBINTEGAP2_MASK    (0x7F << ENET_IPGR_NBTOBINTEGAP1_POS)
#define ENET_IPGR_NBTOBINTEGAP1_MASK    (0x7F << ENET_IPGR_NBTOBINTEGAP2_POS)

/* Recommended value */
#define ENET_IPGR_NBTOBINTEGAP2_DEF     0x12
#define ENET_IPGR_NBTOBINTEGAP1_DEF     0x0C

/* CLRT Register bit definitions */
#define ENET_CLRT_RETRANSMAX_POS        0   /*!< maximum  of retransmission */
#define ENET_CLRT_COLLWIN_POS           8   /*!< collision window */

#define ENET_CLRT_RETRANSMAX_MASK       (0x0F << ENET_CLRT_RETRANSMAX_POS)
#define ENET_CLRT_COLLWIN_MASK          (0x3F << ENET_CLRT_COLLWIN_POS)

/* Default value */
#define ENET_CLRT_RETRANSMAX_DEF        0x0F
#define ENET_CLRT_COLLWIN_DEF           0x37

/* MAXF Register bit definitions */
#define ENET_MAXF_MAXFLEN_POS           0   /*< maximum receive frame */
#define ENET_MAXF_MAXFLEN_MASK          (0xFFFF << ENET_MAXF_LENGTH_POS)

/* Default value */
#define ENET_MAXF_MAXFLEN_DEF           (0x600)

/* SUPP Register bit definitions */
#define ENET_SUPP_100MBPS_SPEED_POS     8   /*!< Reduced MII Logic Current Speed */
#define ENET_SUPP_100MBPS_SPEED_MASK    (1 << ENET_SUPP_100MBPS_SPEED_POS)

/* Test Register bit definitions */
#define ENET_TEST_SCPQ_POS              0   /*!< Shortcut Pause Quanta */
#define ENET_TEST_TESTPAUSE_POS         1   /*!< Test Pause */
#define ENET_TEST_TESTBP_POS            2   /*!< Test Back Pressure */

#define ENET_TEST_SCPQ_MASK             (1 << ENET_TEST_SCPQ_POS)
#define ENET_TEST_TESTPAUSE_MASK        (1 << ENET_TEST_TESTPAUSE_POS)
#define ENET_TEST_TESTBP_MASK           (1 << ENET_TEST_TESTBP_POS)

/* MCFG Register bit definitions */
#define ENET_MCFG_SCANINC_POS           0   /*!< Scan Increment PHY Address */
#define ENET_MCFG_SUPPPREAMBLE_POS      1   /*!< Suppress Preamble */
#define ENET_MCFG_CLOCKSEL_POS          2   /*!< Clock Select Field */
#define ENET_MCFG_RESETMIIMGMT_POS      15  /*!< Reset MII Management Hardware */

#define ENET_MCFG_SCANINC_MASK          (1 << ENET_MCFG_SCANINC_POS)
#define ENET_MCFG_SUPPPREAMBLE_MASK     (1 << ENET_MCFG_SUPPPREAMBLE_POS)
#define ENET_MCFG_CLOCKSEL_MASK         (0x0F << ENET_MCFG_CLOCKSEL_POS)
#define ENET_MCFG_RESETMIIMGMT_MASK     (1 << ENET_MCFG_RESETMIIMGMT_POS)

#define ENET_MCFG_CLOCKSEL_DIV4         0b0000
#define ENET_MCFG_CLOCKSEL_DIV6         0b0010
#define ENET_MCFG_CLOCKSEL_DIV8         0b0011
#define ENET_MCFG_CLOCKSEL_DIV10        0b0100
#define ENET_MCFG_CLOCKSEL_DIV14        0b0101
#define ENET_MCFG_CLOCKSEL_DIV20        0b0110
#define ENET_MCFG_CLOCKSEL_DIV28        0b0111
#define ENET_MCFG_CLOCKSEL_DIV36        0b1000
#define ENET_MCFG_CLOCKSEL_DIV40        0b1001
#define ENET_MCFG_CLOCKSEL_DIV44        0b1010
#define ENET_MCFG_CLOCKSEL_DIV48        0b1011
#define ENET_MCFG_CLOCKSEL_DIV52        0b1100
#define ENET_MCFG_CLOCKSEL_DIV56        0b1101
#define ENET_MCFG_CLOCKSEL_DIV60        0b1110
#define ENET_MCFG_CLOCKSEL_DIV64        0b1111

/* MCMD Register bit definitions */
#define ENET_MCMD_READ_POS              0   /*< trigger MII Read */
#define ENET_MCMD_SCAN_POS              1   /*< MII Scan continuously */

#define ENET_MCMD_READ_MASK             (1 << ENET_MCMD_READ_POS)
#define ENET_MCMD_SCAN_MASK             (1 << ENET_MCMD_SCAN_POS)

/* MADR Register bit definitions */
#define ENET_MADR_REGADDR_POS           0   /*< MII Register Address field */
#define ENET_MADR_PHYADDR_POS           8   /*< PHY Address Field */

#define ENET_MADR_REGADDR_MASK          (0x1F << ENET_MADR_REGADDR_POS)
#define ENET_MADR_PHYADDR_MASK          (0x1F << ENET_MADR_PHYADDR_POS)

/* MWTD Register bit definitions */
#define ENET_MWTD_DATA_POS              0  /*< MII data to write */
#define ENET_MWTD_DATA_MASK             (0xFFFF << ENET_MWTD_DATA_POS)

/* MRDD Register bit definitions */
#define ENET_MRDD_DATA_POS              0  /*< MII data read */
#define ENET_MRDD_DATA_MASK             (0xFFFF << ENET_MWTD_DATA_POS)

/* MIND Register bit definitions */
#define ENET_MIND_BUSY_POS              0   /*!< MII is Busy */
#define ENET_MIND_SCANNING_POS          1   /*!< MII Scanning in Progress */
#define ENET_MIND_NOTVALID_POS          2   /*!< MII Read Data not valid */
#define ENET_MIND_MIILINKFAIL_POS       3   /*!< MII Link Failed */

#define ENET_MIND_BUSY_MASK             (1 << ENET_MIND_BUSY_POS)
#define ENET_MIND_SCANNING_MASK         (1 << ENET_MIND_SCANNING_POS)
#define ENET_MIND_NOTVALID_MASK         (1 << ENET_MIND_NOTVALID_POS)
#define ENET_MIND_MIILINKFAIL_MASK      (1 << ENET_MIND_MIILINKFAIL_POS)

/* Command Register bit definition */
#define ENET_COMMAND_RXENABLE_POS       0   /*!< Enable Receive */
#define ENET_COMMAND_TXENABLE_POS       1   /*!< Enable Transmit */
#define ENET_COMMAND_REGRESET_POS       3   /*!< Reset Host Registers */
#define ENET_COMMAND_TXRESET_POS        4   /*!< Reset Transmit Datapath */
#define ENET_COMMAND_RXRESET_POS        5   /*!< Reset Receive Datapath */
#define ENET_COMMAND_PASSRUNTFRAME_POS  6   /*!< Pass Runt Frames */
#define ENET_COMMAND_PASSRXFILTER_POS   7   /*!< Pass RX Filter */
#define ENET_COMMAND_TXFLOWCONTROL_POS  8   /*!< TX Flow Control */
#define ENET_COMMAND_RMII_POS           9   /*!< Reduced MII Interface */
#define ENET_COMMAND_FULLDUPLEX_POS     10  /*!< Full Duplex */

#define ENET_COMMAND_RXENABLE_MASK      (1 << ENET_COMMAND_RXENABLE_POS)
#define ENET_COMMAND_TXENABLE_MASK      (1 << ENET_COMMAND_TXENABLE_POS)
#define ENET_COMMAND_REGRESET_MASK      (1 << ENET_COMMAND_REGRESET_POS)
#define ENET_COMMAND_TXRESET_MASK       (1 << ENET_COMMAND_TXRESET_POS)
#define ENET_COMMAND_RXRESET_MASK       (1 << ENET_COMMAND_RXRESET_POS)
#define ENET_COMMAND_PASSRUNTFRAME_MASK (1 << ENET_COMMAND_PASSRUNTFRAME_POS)
#define ENET_COMMAND_PASSRXFILTER_MASK  (1 << ENET_COMMAND_PASSRXFILTER_POS)
#define ENET_COMMAND_TXFLOWCONTROL_MASK (1 << ENET_COMMAND_TXFLOWCONTROL_POS)
#define ENET_COMMAND_RMII_MASK          (1 << ENET_COMMAND_RMII_POS)
#define ENET_COMMAND_FULLDUPLEX_MASK    (1 << ENET_COMMAND_FULLDUPLEX_POS)

/* Status Register bit definition */
#define ENET_STATUS_RX_POS              0   /*!< Receive Channel Active Status */
#define ENET_STATUS_TX_POS              1   /*!< Transmit Channel Active Status */

#define ENET_STATUS_RX_MASK             (1 << ENET_STATUS_RX_POS)
#define ENET_STATUS_TX_MASK             (1 << ENET_STATUS_TX_POS)

// TODO: complete below sections

/*
 * @brief Transmit Status Vector 0 Register bit definitions
 */
#define ENET_TSV0_CRCERR        0x00000001	/*!< CRC error */
#define ENET_TSV0_LCE           0x00000002	/*!< Length Check Error */
#define ENET_TSV0_LOR           0x00000004	/*!< Length Out of Range */
#define ENET_TSV0_DONE          0x00000008	/*!< Tramsmission Completed  */
#define ENET_TSV0_MULTICAST     0x00000010	/*!< Multicast Destination */
#define ENET_TSV0_BROADCAST     0x00000020	/*!< Broadcast Destination */
#define ENET_TSV0_PACKETDEFER   0x00000040	/*!< Packet Deferred */
#define ENET_TSV0_EXDF          0x00000080	/*!< Excessive Packet Deferral */
#define ENET_TSV0_EXCOL         0x00000100	/*!< Excessive Collision */
#define ENET_TSV0_LCOL          0x00000200	/*!< Late Collision Occured  */
#define ENET_TSV0_GIANT         0x00000400	/*!< Giant Frame */
#define ENET_TSV0_UNDERRUN      0x00000800	/*!< Buffer Underrun */
#define ENET_TSV0_TOTALBYTES    0x0FFFF000	/*!< Total Bytes Transferred  */
#define ENET_TSV0_CONTROLFRAME  0x10000000	/*!< Control Frame */
#define ENET_TSV0_PAUSE         0x20000000	/*!< Pause Frame */
#define ENET_TSV0_BACKPRESSURE  0x40000000	/*!< Backpressure Method Applied */
#define ENET_TSV0_VLAN          0x80000000	/*!< VLAN Frame */

/*
 * @brief Transmit Status Vector 0 Register bit definitions
 */
#define ENET_TSV1_TBC       0x0000FFFF	/*!< Transmit Byte Count */
#define ENET_TSV1_TCC       0x000F0000	/*!< Transmit Collision Count */

/*
 * @brief Receive Status Vector Register bit definitions
 */
#define ENET_RSV_RBC            0x0000FFFF	/*!< Receive Byte Count */
#define ENET_RSV_PPI            0x00010000	/*!< Packet Previously Ignored */
#define ENET_RSV_RXDVSEEN       0x00020000	/*!< RXDV Event Previously Seen */
#define ENET_RSV_CESEEN         0x00040000	/*!< Carrier Event Previously Seen */
#define ENET_RSV_RCV            0x00080000	/*!< Receive Code Violation */
#define ENET_RSV_CRCERR         0x00100000	/*!< CRC Error */
#define ENET_RSV_LCERR          0x00200000	/*!< Length Check Error */
#define ENET_RSV_LOR            0x00400000	/*!< Length Out of Range */
#define ENET_RSV_ROK            0x00800000	/*!< Frame Received OK */
#define ENET_RSV_MULTICAST      0x01000000	/*!< Multicast Frame */
#define ENET_RSV_BROADCAST      0x02000000	/*!< Broadcast Frame */
#define ENET_RSV_DRIBBLENIBBLE  0x04000000	/*!< Dribble Nibble */
#define ENET_RSV_CONTROLFRAME   0x08000000	/*!< Control Frame */
#define ENET_RSV_PAUSE          0x10000000	/*!< Pause Frame */
#define ENET_RSV_UO             0x20000000	/*!< Unsupported Opcode */
#define ENET_RSV_VLAN           0x40000000	/*!< VLAN Frame */

/*
 * @brief Flow Control Counter Register bit definitions
 */
#define ENET_FLOWCONTROLCOUNTER_MC(n)   ((n) & 0xFFFF)			/*!< Mirror Counter */
#define ENET_FLOWCONTROLCOUNTER_PT(n)   (((n) & 0xFFFF) << 16)	/*!< Pause Timer */

/*
 * @brief Flow Control Status Register bit definitions
 */
#define ENET_FLOWCONTROLSTATUS_MCC(n) ((n) & 0xFFFF)	/*!< Mirror Counter Current            */

/* Receive Filter Control Register bit definitions */
#define ENET_RXFILTERCTRL_AUE_POS           0   /*!< Accept Unicast Frames Enable */
#define ENET_RXFILTERCTRL_ABE_POS           1   /*!< Accept Broadcast Frames Enable */
#define ENET_RXFILTERCTRL_AME_POS           2   /*!< Accept Multicast Frames Enable */
#define ENET_RXFILTERCTRL_AUHE_POS          3   /*!< Accept Unicast Hash Filter Frames */
#define ENET_RXFILTERCTRL_AMHE_POS          4   /*!< Accept Multicast Hash Filter Fram */
#define ENET_RXFILTERCTRL_APE_POS           5   /*!< Accept Perfect Match Enable */
#define ENET_RXFILTERCTRL_MPEW_POS          12  /*!< Magic Packet Filter WoL Enable */
#define ENET_RXFILTERCTRL_RFEW_POS          13  /*!< Perfect Filter WoL Enable */

#define ENET_RXFILTERCTRL_AUE_MASK          (1 << ENET_RXFILTERCTRL_AUE_POS)
#define ENET_RXFILTERCTRL_ABE_MASK          (1 << ENET_RXFILTERCTRL_ABE_POS)
#define ENET_RXFILTERCTRL_AME_MASK          (1 << ENET_RXFILTERCTRL_AME_POS)
#define ENET_RXFILTERCTRL_AUHE_MASK         (1 << ENET_RXFILTERCTRL_AUHE_POS)
#define ENET_RXFILTERCTRL_AMHE_MASK         (1 << ENET_RXFILTERCTRL_AMHE_POS)
#define ENET_RXFILTERCTRL_APE_MASK          (1 << ENET_RXFILTERCTRL_APE_POS)
#define ENET_RXFILTERCTRL_MPEW_MASK         (1 << ENET_RXFILTERCTRL_MPEW_POS)
#define ENET_RXFILTERCTRL_RFEW_MASK         (1 << ENET_RXFILTERCTRL_RFEW_POS)
/*
 * @brief Receive Filter WoL Status/Clear Register bit definitions
 */
#define ENET_RXFILTERWOLSTATUS_AUW          0x00000001	/*!< Unicast Frame caused WoL */
#define ENET_RXFILTERWOLSTATUS_ABW          0x00000002	/*!< Broadcast Frame caused WoL */
#define ENET_RXFILTERWOLSTATUS_AMW          0x00000004	/*!< Multicast Frame caused WoL */
#define ENET_RXFILTERWOLSTATUS_AUHW         0x00000008	/*!< Unicast Hash Filter Frame WoL */
#define ENET_RXFILTERWOLSTATUS_AMHW         0x00000010	/*!< Multicast Hash Filter Frame WoL */
#define ENET_RXFILTERWOLSTATUS_APW          0x00000020	/*!< Perfect Filter WoL */
#define ENET_RXFILTERWOLSTATUS_RFW          0x00000080	/*!< RX Filter caused WoL */
#define ENET_RXFILTERWOLSTATUS_MPW          0x00000100	/*!< Magic Packet Filter caused WoL */
#define ENET_RXFILTERWOLSTATUS_BITMASK      0x01BF		/*!< Receive Filter WoL Status/Clear bitmasl value */

/*
 * @brief Interrupt Status/Enable/Clear/Set Register bit definitions
 */
#define ENET_INT_RXOVERRUN      0x00000001	/*!< Overrun Error in RX Queue */
#define ENET_INT_RXERROR        0x00000002	/*!< Receive Error */
#define ENET_INT_RXFINISHED     0x00000004	/*!< RX Finished Process Descriptors */
#define ENET_INT_RXDONE         0x00000008	/*!< Receive Done */
#define ENET_INT_TXUNDERRUN     0x00000010	/*!< Transmit Underrun */
#define ENET_INT_TXERROR        0x00000020	/*!< Transmit Error */
#define ENET_INT_TXFINISHED     0x00000040	/*!< TX Finished Process Descriptors */
#define ENET_INT_TXDONE         0x00000080	/*!< Transmit Done */
#define ENET_INT_SOFT           0x00001000	/*!< Software Triggered Interrupt */
#define ENET_INT_WAKEUP         0x00002000	/*!< Wakeup Event Interrupt */

#endif /* ENET_REG_H */
