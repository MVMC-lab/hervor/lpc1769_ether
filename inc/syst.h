/**
 * @file syst.h
 * @author LiYu87@mvmc-lab
 * @brief Systick driver
 * @date 2020.04.10
 *
 * SysTick_Handler is used in syst.c
 */

#ifndef SYST_H
#define SYST_H

#include <inttypes.h>

/**
 * @brief Initialize systick as 0.1ms and enable interrupt
 */
void syst_init(void);

/**
 * @brief Get the current systick time in 0.1 ms.
 *
 * @return current systick time (0.1ms).
 */
uint32_t syst_get(void);

#endif  /* SYST_H */
