/**
 * @file ible.h
 * @author LiYu87@mvmc-lab
 * @brief Interface between LWIP and Ethernet MAC
 * @date 2020.04.13
 */

#ifndef IBLE_H
#define IBLE_H

#include <lwip/netif.h>

void ible_input(struct netif *netif_p);
err_t ible_netif_init(struct netif *netif_p);

#endif /* IBLE_H */
