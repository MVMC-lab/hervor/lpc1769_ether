/**
 * @file enet_cfg.h
 * @author LiYu87@mvmc-lab
 * @brief Config for ethernet driver
 * @date 2020.04.07
 */

#ifndef ENET_CFG_H
#define ENET_CFG_H

#include <inttypes.h>

/**
 * @brief Defines the number of descriptors used for RX
 */
#define ENET_NUM_TX_DESCS 4

/**
 * @brief Defines the number of descriptors used for TX
 */
#define ENET_NUM_RX_DESCS 4

/**
 * @brief Maximum size of an ethernet buffer
 */
#define ENET_ETH_MAX_FLEN (1536)


#if ENET_NUM_TX_DESCS < 2
#error ENET_NUM_TX_DESCS must be at least 2
#endif

#if ENET_NUM_RX_DESCS < 3
#error ENET_NUM_TX_DESCS must be at least 3
#endif

#define ENET_PHY_TYPE_DP8

#endif /* ENET_CFG_H */
