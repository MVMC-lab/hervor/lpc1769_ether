/**
 * @file enet.h
 * @author LiYu87@mvmc-lab
 * @brief Ethernet driver
 * @date 2020.04.07
 *
 * For tx struct and memery layout, see "10.15.2 Transmit descriptors
 * and statuses" in UM10360.
 *
 * For rx struct and memery layout, see "10.15.1 Receive descriptors
 * and statuses" in UM10360.
 *
 * TODO: use union to cast Control?
 * TODO: function comment?
 */

#ifndef ENET_H
#define ENET_H

#include <inttypes.h>

#include "enet_cfg.h"
#include "enet_reg.h"
#include "enet_phy.h"

/**
 * @brief TX Descriptor Structure
 *
 * This struct abstrac layer for TX data memory handling.
 * Use this structure to access memory and never read/write
 * memory directly.
 *
 * Packet is an address of the TX data buffer. Driver write to
 * Packet, and hardware read data out from this buffer.
 *
 * Control handle the information of the TX Descriptor,
 * e.g. buffer size, frame end flag. Driver write this flied to
 * control TX frame. See "Table 182. Transmit descriptor control
 * word".
 *
 * +-----------------------------------------------------------+
 * | Interrupt | Last | CRC | Pad | Huge | Override | - | Size |
 * |    31     |  30  |  29 |  28 |  27  |    26    |   | 10:0 |
 * +-----------------------------------------------------------+
 *
 * NOTE: Size should length of TX data buffer minus 1.
 */
typedef struct {
    uint32_t Packet;  /*!< Base address of the TX data buffer */
    uint32_t Control;  /*!< Control information */
} EnetTxDesc_t;

/* TX Descriptor Control Word definition */
#define ENET_TCTRL_SIZE_POS             0
#define ENET_TCTRL_OVERRIDE_POS         26
#define ENET_TCTRL_HUGE_POS             27
#define ENET_TCTRL_PAD_POS              28
#define ENET_TCTRL_CRC_POS              29
#define ENET_TCTRL_LAST_POS             30
#define ENET_TCTRL_INTERRUPT_POS        31

#define ENET_TCTRL_SIZE_MASK            (0x7FF << ENET_TCTRL_SIZE_POS)  /*!< 10 bits */
#define ENET_TCTRL_OVERRIDE_MASK        (1 << ENET_TCTRL_OVERRIDE_POS)
#define ENET_TCTRL_HUGE_MASK            (1 << ENET_TCTRL_HUGE_POS)
#define ENET_TCTRL_PAD_MASK             (1 << ENET_TCTRL_PAD_POS)
#define ENET_TCTRL_CRC_MASK             (1 << ENET_TCTRL_CRC_POS)
#define ENET_TCTRL_LAST_MASK            (1 << ENET_TCTRL_LAST_POS)
#define ENET_TCTRL_INTERRUPT_MASK       (1 << ENET_TCTRL_INTERRUPT_POS)

/**
 * @brief TX Status Structure
 *
 * See "Table 183. Transmit status fields".
 */
typedef struct {
    uint32_t StatusInfo; /*!< Receive status return flags.*/
} EnetTxStat_t;

/* TX Status Information Word definition */
#define ENET_TINFO_COL_CNT_POS          21  /*< the number of collisions */
#define ENET_TINFO_DEFER_POS            25
#define ENET_TINFO_EXCESS_DEF_POS       26
#define ENET_TINFO_EXCESS_COL_POS       27
#define ENET_TINFO_LATE_COL_POS         28
#define ENET_TINFO_UNDERRUN_POS         29
#define ENET_TINFO_NO_DESCR_POS         30
#define ENET_TINFO_ERROR_POS            31

#define ENET_TINFO_COL_CNT_MASK         (0x0F << ENET_TINFO_COL_CNT_POS)  /*!< 4 bits */
#define ENET_TINFO_DEFER_MASK           (1 << ENET_TINFO_DEFER_POS)
#define ENET_TINFO_EXCESS_DEF_MASK      (1 << ENET_TINFO_EXCESS_DEF_POS)
#define ENET_TINFO_EXCESS_COL_MASK      (1 << ENET_TINFO_EXCESS_COL_POS)
#define ENET_TINFO_LATE_COL_MASK        (1 << ENET_TINFO_LATE_COL_POS)
#define ENET_TINFO_UNDERRUN_MASK        (1 << ENET_TINFO_UNDERRUN_POS)
#define ENET_TINFO_NO_DESCR_MASK        (1 << ENET_TINFO_NO_DESCR_POS)
#define ENET_TINFO_ERROR_MASK           (1 << ENET_TINFO_ERROR_POS)

/**
 * @brief RX Descriptor Structure
 *
 * This struct abstrac layer for RX data memory handling.
 * Use this structure to access memory and never read/write
 * memory directly.
 *
 * Control handle the information of the RX Descriptor,
 * e.g. buffer size, frame end flag. Driver write this flied to
 * control TX frame. See "Table 182. Transmit descriptor control
 * word".
 */
typedef struct {
    uint32_t Packet;  /*!< Base address of the rx data buffer */
    uint32_t Control; /*!< Control information */
} EnetRxDesc_t;

/* RX Descriptor Control Word definition */
#define ENET_RCTRL_SIZE_POS             0
#define ENET_RCTRL_INTERRUPT_POS        31

#define ENET_RCTRL_SIZE_MASK            (0x7FF << ENET_RCTRL_SIZE_POS)  /*!< 10 bits */
#define ENET_RCTRL_INTERRUPT_MASK       (1 << ENET_RCTRL_INTERRUPT_POS)

/**
 * @brief RX Status Structure
 *
 * See "Table 179. Receive Status HashCRC Word".
 */
typedef struct {
    uint32_t StatusInfo;
    uint32_t StatusHashCRC;
} EnetRxStat_t;

/* RX Status Hash CRC Word definition */
#define ENET_RHASH_SA_POS               0   /*!< Hash CRC for the source address. */
#define ENET_RHASH_DA_POS               16  /*!< Hash CRC for the destination address. */

#define ENET_RHASH_SA_MASK              (0x1FF << ENET_RHASH_SA_POS) /*!< 9 bits */
#define ENET_RHASH_DA_MASK              (0x1FF << ENET_RHASH_DA_POS) /*!< 9 bits */

/* RX Status Information Word definition */
#define ENET_RINFO_SIZE_POS             0
#define ENET_RINFO_CTL_FRM_POS          18
#define ENET_RINFO_VLAN_POS             19
#define ENET_RINFO_FAIL_FILTER_POS      20
#define ENET_RINFO_MULTICAST_POS        21
#define ENET_RINFO_BROADCAST_POS        22
#define ENET_RINFO_CRC_ERR_POS          23
#define ENET_RINFO_SYM_ERR_POS          24
#define ENET_RINFO_LEN_ERR_POS          25
#define ENET_RINFO_RANGE_ERR_POS        26
#define ENET_RINFO_ALIGN_ERR_POS        27
#define ENET_RINFO_OVERRUN_POS          28
#define ENET_RINFO_NO_DESCR_POS         29
#define ENET_RINFO_LAST_FLAG_POS        30
#define ENET_RINFO_ERR_POS              31

#define ENET_RINFO_SIZE_MASK            (0x7FF << ENET_RINFO_SIZE_POS) /*!< 10 bits */
#define ENET_RINFO_CTL_FRM_MASK         (1 << ENET_RINFO_CTL_FRM_POS)
#define ENET_RINFO_VLAN_MASK            (1 << ENET_RINFO_VLAN_POS)
#define ENET_RINFO_FAIL_FILTER_MASK     (1 << ENET_RINFO_FAIL_FILTER_POS)
#define ENET_RINFO_MULTICAST_MASK       (1 << ENET_RINFO_MULTICAST_POS)
#define ENET_RINFO_BROADCAST_MASK       (1 << ENET_RINFO_BROADCAST_POS)
#define ENET_RINFO_CRC_ERR_MASK         (1 << ENET_RINFO_CRC_ERR_POS)
#define ENET_RINFO_SYM_ERR_MASK         (1 << ENET_RINFO_SYM_ERR_POS)
#define ENET_RINFO_LEN_ERR_MASK         (1 << ENET_RINFO_LEN_ERR_POS)
#define ENET_RINFO_RANGE_ERR_MASK       (1 << ENET_RINFO_RANGE_ERR_POS)
#define ENET_RINFO_ALIGN_ERR_MASK       (1 << ENET_RINFO_ALIGN_ERR_POS)
#define ENET_RINFO_OVERRUN_MASK         (1 << ENET_RINFO_OVERRUN_POS)
#define ENET_RINFO_NO_DESCR_MASK        (1 << ENET_RINFO_NO_DESCR_POS)
#define ENET_RINFO_LAST_FLAG_MASK       (1 << ENET_RINFO_LAST_FLAG_POS)
#define ENET_RINFO_ERR_MASK             (1 << ENET_RINFO_ERR_POS)

void enet_reset(void);
void enet_init(void);
void enet_deinit(void);
void enet_tx_enable(void);
void enet_tx_disable(void);
void enet_tx_reset(void);
void enet_rx_enable(void);
void enet_rx_disable(void);
void enet_rx_reset(void);
void enet_set_addr(const uint8_t *mac_p);

void enet_mii_init(void);
uint8_t enet_mii_is_busy(void);
void enet_mii_start_read(uint8_t phy, uint8_t reg);
uint16_t enet_mii_read_data(void);
uint16_t enet_mii_read_w(uint8_t phy, uint8_t reg);
void enet_mii_start_write(uint8_t phy, uint8_t reg, uint16_t data);
void enet_mii_write_w(uint8_t phy, uint8_t reg, uint16_t data);
void enet_descriptors_init(void);
void enet_phy_reset(uint8_t phy);
void enet_setup_by_physt(uint16_t physt);

extern EnetTxDesc_t TxDescs[ENET_NUM_TX_DESCS];
extern EnetTxStat_t TxStats[ENET_NUM_TX_DESCS];
extern EnetRxDesc_t RxDescs[ENET_NUM_RX_DESCS];
extern EnetRxStat_t RxStats[ENET_NUM_RX_DESCS];
extern uint8_t TxBuffer[ENET_NUM_TX_DESCS][ENET_ETH_MAX_FLEN];
extern uint8_t RxBuffer[ENET_NUM_RX_DESCS][ENET_ETH_MAX_FLEN];

#endif /* ENET_H */
