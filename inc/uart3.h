/**
 * @file uart3.h
 * @author LiYu87@mvmc-lab
 * @brief uart3 driver
 * @date 2020.04.14
 */

#ifndef UART_H
#define UART_H

#include <inttypes.h>

/**
 * @brief send a byte data through uart3
 *
 * @param data byte data
 */
void uart3_putc(uint8_t data);

/**
 * @brief get a byte data through uart3
 *
 * @param data byte data
 */
uint8_t uart3_getc(void);

#endif  // UART_H
