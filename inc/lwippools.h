/**
 * @file lwippools.h
 * @author LiYu87@mvmc-lab
 * @brief define pools for LWIP using
 * @date 2020.04.10
 */

LWIP_MALLOC_MEMPOOL_START
LWIP_MALLOC_MEMPOOL(5, 256)
LWIP_MALLOC_MEMPOOL(5, 512)
LWIP_MALLOC_MEMPOOL(5, 1512)
LWIP_MALLOC_MEMPOOL_END
