/**
 * @file echo.h
 * @author LiYu87@mvmc-lab
 * @brief TCP echo server example using raw API.
 * @date 2020.04.24
 */

#ifndef ECHO_H
#define ECHO_H

void echo_init(void);

#endif /* ECHO_H */
