/**
 * @file enet_phy.h
 * @author LiYu87@mvmc-lab
 * @brief Ethernet PHY register define
 * @date 2020.04.08
 *
 * Reference: clause 22 in 802.3
 */

#ifndef ENET_PHY_H
#define ENET_PHY_H

#include "enet_cfg.h"

#define PHY_BCR         0x00  /*< (Basic) Control Register */
#define PHY_BSR         0x01  /*< (Basic) Status Register */
#define PHY_PHYID1      0x02  /*< PHY Identifier 1 */
#define PHY_PHYID2      0x03  /*< PHY Identifier 2 */
#define PHY_ANAR        0x04  /*< Auto-Negotiation Advertisement Register */
#define PHY_ANLPAR      0x05  /*< Auto-Negotiation Link Partner Ability Register */
#define PHY_ANER        0x06  /*< Auto-Negotiation Expansion Register */
#define PHY_ANNPTR      0x07  /*< Auto-Negotiation Next Page Transmit Register */

/* PHY BCR register bit definitions */
#define PHY_BCR_RESET_POS              15
#define PHY_BCR_LOOPBACK_POS           14
#define PHY_BCR_SPEED_SELECT_POS       13
#define PHY_BCR_AUTONEG_POS            12
#define PHY_BCR_POWER_DOWN_POS         11
#define PHY_BCR_ISOLATE_POS            10
#define PHY_BCR_RESTART_AUTONEG_POS    9
#define PHY_BCR_DUPLEX_MODE_POS        8

#define PHY_BCR_RESET_MASK             (1 << PHY_BCR_RESET_POS)
#define PHY_BCR_LOOPBACK_MASK          (1 << PHY_BCR_LOOPBACK_POS)
#define PHY_BCR_SPEED_SELECT_MASK      (1 << PHY_BCR_SPEED_SELECT_POS)
#define PHY_BCR_AUTONEG_MASK           (1 << PHY_BCR_AUTONEG_POS)
#define PHY_BCR_POWER_DOWN_MASK        (1 << PHY_BCR_POWER_DOWN_POS)
#define PHY_BCR_ISOLATE_MASK           (1 << PHY_BCR_ISOLATE_POS)
#define PHY_BCR_RESTART_AUTONEG_MASK   (1 << PHY_BCR_RESTART_AUTONEG_POS)
#define PHY_BCR_DUPLEX_MODE_MASK       (1 << PHY_BCR_DUPLEX_MODE_POS)

/* PHY BSR register bit definitions */
#define PHY_BSR_100BASE_T4_POS          (15)   /*!< T4 mode */
#define PHY_BSR_100BASE_TX_FD_POS       (14)   /*!< 100MBps full duplex */
#define PHY_BSR_100BASE_TX_HD_POS       (13)   /*!< 100MBps half duplex */
#define PHY_BSR_10BASE_T_FD_POS         (12)   /*!< 100Bps full duplex */
#define PHY_BSR_10BASE_T_HD_POS         (11)   /*!< 10MBps half duplex */
#define PHY_BSR_AUTONEG_COMP_POS        (5)    /*!< Auto-negotation complete */
#define PHY_BSR_RMT_FAULT_POS           (4)    /*!< Fault */
#define PHY_BSR_AUTONEG_ABILITY_POS     (3)    /*!< Auto-negotation supported */
#define PHY_BSR_LINK_STATUS_POS         (2)    /*!< 1=Link active */
#define PHY_BSR_JABBER_DETECT_POS       (1)    /*!< Jabber detect */
#define PHY_BSR_EXTEND_CAPAB_POS        (0)    /*!< Supports extended capabilities */

#define PHY_BSR_100BASE_T4_MASK         (1 << PHY_BSR_100BASE_T4_POS)
#define PHY_BSR_100BASE_TX_FD_MASK      (1 << PHY_BSR_100BASE_TX_FD_POS)
#define PHY_BSR_100BASE_TX_HD_MASK      (1 << PHY_BSR_100BASE_TX_HD_POS)
#define PHY_BSR_10BASE_T_FD_MASK        (1 << PHY_BSR_10BASE_T_FD_POS)
#define PHY_BSR_10BASE_T_HD_MASK        (1 << PHY_BSR_10BASE_T_HD_POS)
#define PHY_BSR_AUTONEG_COMP_MASK       (1 << PHY_BSR_AUTONEG_COMP_POS)
#define PHY_BSR_RMT_FAULT_MASK          (1 << PHY_BSR_RMT_FAULT_POS)
#define PHY_BSR_AUTONEG_ABILITY_MASK    (1 << PHY_BSR_AUTONEG_ABILITY_POS)
#define PHY_BSR_LINK_STATUS_MASK        (1 << PHY_BSR_LINK_STATUS_POS)
#define PHY_BSR_JABBER_DETECT_MASK      (1 << PHY_BSR_JABBER_DETECT_POS)
#define PHY_BSR_EXTEND_CAPAB_MASK       (1 << PHY_BSR_EXTEND_CAPAB_POS)

#ifdef ENET_PHY_TYPE_DP8
#define PHY_DP8_PHYSTS      0x10  /*< PHY Status Register */
#define PHY_DP8_MICR        0x11  /*< MII Interrupt Control Register */
#define PHY_DP8_MISR        0x12  /*< MII Interrupt Status and Misc. Control Register */
#define PHY_DP8_FCSCR       0x14  /*< False Carrier Sense Counter Register */
#define PHY_DP8_RECR        0x15  /*< Receive Error Counter Register */
#define PHY_DP8_PCSR        0x16  /*< PCS Sub-Layer Configuration and Status Register */
#define PHY_DP8_RBR         0x17  /*< RMII and Bypass Register */
#define PHY_DP8_LEDCR       0x18  /*< LED Direct Control Register */
#define PHY_DP8_PHYCR       0x19  /*< PHY Control Register */
#define PHY_DP8_10BT_SERIAL 0x1A  /*< 10Base-T Status/Control Register*/
#define PHY_DP8_CDCTRL1     0x1B  /*< CD Test Control and BIST Extensions Register */
#define PHY_DP8_EDCR        0x1D  /*< Energy Detect Control Register */
#endif /* ENET_PHY_TYPE */


#endif /* ENET_PHY_H */
