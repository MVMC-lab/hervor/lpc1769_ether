/**
 * @file system.c
 * @author LiYu87@mvmc-lab
 * @date 2020.03.03
 * @brief system initailize and deinitailize function.
 */

#ifndef SYSTEM_H
#define SYSTEM_H

void system_init(void);

void system_deinit(void);

#endif  // SYSTEM_H
