/**
 * @file sys_arch.h
 * @author LiYu87@mvmc-lab
 * @brief Provides semaphores and mailboxes to lwIP.
 * @date 2020.04.10
 *
 * NOTE: current project is NO_SYS, so this is not necessary
 */

#ifndef LWIP_ARCH_SYS_ARCH_H
#define LWIP_ARCH_SYS_ARCH_H

#define SYS_MBOX_NULL   NULL
#define SYS_SEM_NULL    NULL

typedef void * sys_prot_t;

typedef void * sys_sem_t;

typedef void * sys_mbox_t;

typedef void * sys_thread_t;

#endif /* LWIP_ARCH_SYS_ARCH_H */
