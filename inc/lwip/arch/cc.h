/**
 * @file cc.h
 * @author LiYu87@mvmc-lab
 * @brief Describes compiler and processor to lwIP.
 * @date 2020.04.10
 *
 * reference : https://lwip.fandom.com/wiki/Porting_for_an_OS
 * reference : https://lwip.fandom.com/wiki/Porting_For_Bare_Metal
 * see lib\lwip\src\include\lwip\def.h
 *
 * TODO: Is LWIP_PLATFORM_BYTESWAP necessary?
 */

#ifndef LWIP_ARCH_CC_H
#define LWIP_ARCH_CC_H

#include <stdint.h>
#include <stdio.h>

/* Types based on stdint.h */
typedef uint8_t            u8_t;
typedef int8_t             s8_t;
typedef uint16_t           u16_t;
typedef int16_t            s16_t;
typedef uint32_t           u32_t;
typedef int32_t            s32_t;
typedef uintptr_t          mem_ptr_t;

/* Define (sn)printf formatters for these lwIP types */
#define X8_F  "02x"
#define U16_F "hu"
#define S16_F "hd"
#define X16_F "hx"
#define U32_F "lu"
#define S32_F "ld"
#define X32_F "lx"
#define SZT_F "uz"

/* LPC1769 is little endian only */
#define BYTE_ORDER LITTLE_ENDIAN

/* checksums algorithms */
#define LWIP_CHKSUM_ALGORITHM 1


/* Use LWIP error codes */
#define LWIP_PROVIDE_ERRNO

/* structure packing setting */
#if defined(__arm__) && defined(__ARMCC_VERSION)
    /* Keil uVision tools */
    #define PACK_STRUCT_BEGIN __packed
    #define PACK_STRUCT_STRUCT
    #define PACK_STRUCT_END
    #define PACK_STRUCT_FIELD(fld) fld
    #define ALIGNED(n)  __align(n)
#elif defined (__IAR_SYSTEMS_ICC__)
    /* IAR Embedded Workbench tools */
    #define PACK_STRUCT_BEGIN __packed
    #define PACK_STRUCT_STRUCT
    #define PACK_STRUCT_END
    #define PACK_STRUCT_FIELD(fld) fld
    #define ALIGNEDX(x)      _Pragma(#x)
    #define ALIGNEDXX(x)     ALIGNEDX(data_alignment=x)
    #define ALIGNED(x)       ALIGNEDXX(x)
#else
    /* GCC tools (CodeSourcery) */
    #define PACK_STRUCT_BEGIN
    #define PACK_STRUCT_STRUCT __attribute__ ((__packed__))
    #define PACK_STRUCT_END
    #define PACK_STRUCT_FIELD(x) x
    #define ALIGNED(n)  __attribute__((aligned (n)))
#endif


#ifdef LWIP_DEBUG
/**
 * @brief	Displays an error message on assertion
 * @param	msg		: Error message to display
 * @param	line	: Line number in file with error
 * @param	file	: Filename with error
 * @return	Nothing
 * @note	This function will display an error message on an assertion
 * to the debug output.
 */
void assert_printf(char *msg, int line, char *file);

/* Plaform specific diagnostic output */
#define LWIP_PLATFORM_DIAG(vars) printf vars
#define LWIP_PLATFORM_ASSERT(flag) { assert_printf((flag), __LINE__, __FILE__); }
#else

/**
 * @brief	LWIP optimized assertion loop (no LWIP_DEBUG)
 * @return	DoesnNothing, function doesn't return
 */
void assert_loop(void);
#define LWIP_PLATFORM_DIAG(msg) { ; }
#define LWIP_PLATFORM_ASSERT(flag) { assert_loop(); }
#endif

#endif /* LWIP_ARCH_CC_H */
