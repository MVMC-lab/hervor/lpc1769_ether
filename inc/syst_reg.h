/**
 * @file syst_reg.h
 * @author LiYu87@mvmc-lab
 * @brief systick register bit definition
 * @date 2020.03.19
 */

#ifndef SYST_REG_H
#define SYST_REG_H

/**
 * @brief SYST_CSR Register bit definition
 *
 * 4.4.1 SysTick Control and Status Register
 */
#define SYST_CSR_COUNTFLAG_POS      16  /*< Returns 1 if timer counted to 0 since last time this was read. */
#define SYST_CSR_CLKSOURCE_POS      2  /*< 0 = external clock, 1 = processor clock. */
#define SYST_CSR_TICKINT_POS        1  /*< Enables SysTick exception request */
#define SYST_CSR_ENABLE_POS         0  /*< Enables the counter */

#define SYST_CSR_COUNTFLAG_MASK     (1 << SYST_CSR_COUNTFLAG_POS)
#define SYST_CSR_CLKSOURCE_MASK     (1 << SYST_CSR_CLKSOURCE_POS)
#define SYST_CSR_TICKINT_MASK       (1 << SYST_CSR_TICKINT_POS)
#define SYST_CSR_ENABLE_MASK        (1 << SYST_CSR_ENABLE_POS)

/**
 * @brief SYST_RVR Register bit definition
 *
 * 4.4.2 SysTick Reload Value Register
 */
#define SYST_RVR_RELOAD_POS         23  /*< Returns 1 if timer counted to 0 since last time this was read. */
#define SYST_RVR_RELOAD_MASK        (0xFFFFF << SYST_CSR_COUNTFLAG_POS) /*< 24 bits */

/**
 * @brief SYST_CVR Register bit definition
 *
 * 4.4.3 SysTick Current Value Register
 */
#define SYST_CVR_CURRENT_POS        23  /*< Returns 1 if timer counted to 0 since last time this was read. */
#define SYST_CVR_CURRENT_MASK       (0xFFFFF << SYST_CSR_COUNTFLAG_POS) /*< 24 bits */

/**
 * @brief SYST_CALIB Register bit definition
 *
 * 4.4.4 SysTick Calibration Value Register
 */
#define SYST_CALIB_NOREF_POS        31  /*< Indicates a reference clock to the processor. */
#define SYST_CALIB_SKEW_POS         30  /*< Indicates whether the TENMS value. */
#define SYST_CALIB_TENMS_POS        0   /*< Reload value for 10ms (100Hz) timing */

#define SYST_CALIB_NOREF_MASK       (1 << SYST_CALIB_NOREF_POS)
#define SYST_CALIB_SKEW_MASK        (1 << SYST_CALIB_SKEW_POS)
#define SYST_CALIB_TENMS_MASK       (0xFFFFF << SYST_CSR_COUNTFLAG_POS) /*< 24 bits */

#endif /* SYST_REG_H */
